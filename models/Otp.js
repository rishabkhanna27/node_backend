const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const OtpModel = new Schema({
    otp: {
        type: String,
        default: "",
        trim: true
    },
    sid: {
        type: String,
        default: "",
        trim: true
    },
    orderId: {
        type: ObjectId,
        ref: "Order"
    },
    phone: {
        type: String,
        trim: true,
        default: ""
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        default: ""
    },
    countryCode: {
        type: String,
        default: "",
        trim: true
    },
    expiredAt: {
        type: Date,
        default: new Date()
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});
const Otp = mongoose.model('Otp', OtpModel);
module.exports = Otp;