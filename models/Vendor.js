const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");
const ObjectId = mongoose.Types.ObjectId;

let vendorSchema = new Schema({
  firstName: {
    type: String,
    default: ""
  },
  lastName: {
    type: String,
    default: ""
  },
  about: {
    type: String,
    default: ""
  },
  email: {
    type: String,
    default: "",
    lowercase: true
  },
  phoneNo: {
    type: String,
    default: ""
  },
  dialCode: {
    type: String,
    default: ""
  },
  age: {
    type: Number
  },
  gender: {
    type: String,
    enum: ["", "MALE", "FEMALE", "BOTH"],
    default: ""
  },
  serviceFor: {
    type: String,
    enum: ["", "MALE", "FEMALE", "BOTH"],
    default: ""
  },
  image: {
    type: String,
    default: ""
  },
  workImage: [{
    type: String,
    default: ""
  }],
  startTime: {
    type: String,
    default: ""
  },
  type: {
    type: Number,
    default: 3
  },
  endTime: {
    type: String,
    default: ""
  },
  startDate: {
    type: String,
    default: ""
  },
  endDate: {
    type: String,
    default: ""
  },
  breakSlots: [{
    breakTime: {
      type: String,
      default: ""
    },
    breakEnd: {
      type: String,
      default: ""
    }
  }],
  breakTime: {
    type: String,
    default: ""
  },
  breakEnd: {
    type: String,
    default: ""
  },
  streetName: {
    type: String,
    default: ""
  },
  city: {
    type: String,
    default: ""
  },
  state: {
    type: String,
    default: ""
  },
  completeAddress: {
    type: String,
    default: ""
  },
  place: {
    type: String,
    default: ""
  },
  pincode: {
    type: String,
    default: ""
  },
  rating: {
    type: Number,
    default: 0
  },
  latitude: {
    type: Number,
    default: 0
  },
  longitude: {
    type: Number,
    default: 0
  },
  location: {
    type: {
      type: String,
      default: "Point"
    },
    coordinates: {
      type: [Number],
      default: [0, 0]
    }
  },
  isNotification: {
    type: Boolean,
    default: false
  },
  isPhoneVerified: {
    type: Boolean,
    default: false
  },
  isEmailVerified: {
    type: Boolean,
    default: false
  },
  isBlocked: {
    type: Boolean,
    default: false
  },
  isBankAdded: {
    type: Boolean,
    default: false
  },
  isService: {
    type: Boolean,
    default: false
  },
  isTime: {
    type: Boolean,
    default: false
  },
  isDocVerified: {
    type: Boolean,
    default: false
  },
  isDocuments: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  jti: {
    type: String,
    default: "",
    select: false,
    index: true
  },
  password: {
    type: String,
    default: ""
  },
  deviceToken: {
    type: String,
    default: "",
    index: true
  },
  deviceType: {
    type: String,
    default: "",
    enum: ["", "WEB", "ANDROID", "IOS"]
  },
  loginCount: {
    type: Number,
    default: 0
  },
  bankName: {
    type: String,
    default: ""
  },
  accountNo: {
    type: String,
    default: ""
  },
  socketId: {
    type: String,
    default: ""
  },
  swiftCode: {
    type: String,
    default: ""
  },
  stripeConnectId: {
    type: String,
    default: ""
  },
  licences: [{
    type: String
  }],
  documents: [{
    type: String
  }],
  referenceImages: [{
    type: String
  }],
  categoryId: [{
    type: ObjectId,
    ref: "Category"
  }],
  productId: [{
    type: ObjectId,
    ref: "Product"
  }],
  addOnId: [{
    type: ObjectId,
    ref: "Product"
  }],

  geoFence: [{
    type: ObjectId,
    ref: "GeoFence"
  }]
}, {
  timestamps: true
});
vendorSchema.index({
  location: '2dsphere'
});


vendorSchema.methods.authenticate = function (password, callback) {
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error("MISSING_PASSWORD"));

    bcrypt.compare(password, this.password, (error, result) => {
      if (!result) {
        reject(new Error("Invalid Password"));
      }
      resolve(this);
    });
  });

  if (typeof callback != "function") return promise;
  promise
    .then((result) => callback(null, result))
    .catch((err) => callback(err));
};

vendorSchema.methods.setPassword = function (password, callback) {
  console.log("inin");
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error("Missing Password"));

    bcrypt.hash(password, 10, (err, hash) => {
      if (err) reject(err);
      this.password = hash;
      resolve(this);
    });
  });

  if (typeof callback != "function") return promise;
  promise
    .then((result) => callback(null, result))
    .catch((err) => callback(err));
};

vendorSchema.pre(/save|create|update/i, function (next) {
  if (this.get("latitude") && this.get("longitude")) {
    const longitude = this.get("longitude");
    const latitude = this.get("latitude");
    const location = {
      type: "Point",
      coordinates: [parseFloat(longitude), parseFloat(latitude)]
    };
    this.set({
      location
    });
  }
  next();
});

module.exports = mongoose.model('Vendor', vendorSchema);