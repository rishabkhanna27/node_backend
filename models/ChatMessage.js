const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;
const constants = require("../common/constants");

const chatMessageModel = new Schema({
    orderId: {
        type: ObjectId,
        ref: "Order",
        default: null
    },
    senderId: {
        type: String
    },
    message: {
        type: String
    },
    messageType: {
        type: Number,
        enum: [constants.MESSAGE_TYPE.MESSAGE, constants.MESSAGE_TYPE.IMAGE, constants.MESSAGE_TYPE.VIDEO],
        default: constants.MESSAGE_TYPE.MESSAGE
    },
    isUserRead: {
        type: Boolean,
        default: false
    },
    isDriverRead: {
        type: Boolean,
        default: false
    },
    userId: {
        type: ObjectId,
        ref: "User",
        default: null
    },
    driverId: {
        type: ObjectId,
        ref: "Driver",
        default: null
    },
    isDeleted: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});
const chatMessage = mongoose.model('chatMessage', chatMessageModel);
module.exports = chatMessage;