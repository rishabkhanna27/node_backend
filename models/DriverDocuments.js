const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documents = new Schema({
    driverId: {
        type: mongoose.Types.ObjectId,
        ref: 'Driver'
    },
    documentId: {
        type: mongoose.Types.ObjectId,
        ref: 'requiredDocument'
    },
    image: {
        type: String,
        default: ""
    },
    backImage: {
        type: String,
        default: ""
    },
    certificateNumber: {
        type: String,
        default: ""
    },
    issueDate: {
        type: Date
    },
    expiryDate: {
        type: Date
    },
    isVerify: {
        type: Boolean,
        default: false
    },
    isReject: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});
documents.virtual('requiredDocument', {
    ref: 'requiredDocument',
    localField: 'documentId',
    foreignField: '_id',
    justOne: true
});
const driverDocument = mongoose.model('driverDocuments', documents);
module.exports = driverDocument;