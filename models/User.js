const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");

let userSchema = new Schema({
  firstName: {
    type: String,
    default: ""
  },
  lastName: {
    type: String,
    default: ""
  },
  email: {
    type: String,
    default: "",
    lowercase: true
  },
  phoneNo: {
    type: String,
    default: ""
  },
  dialCode: {
    type: String,
    default: ""
  },
  gender: {
    type: String,
    enum: ["", "MALE", "FEMALE", "OTHER"],
    default: ""
  },
  image: {
    type: String,
    default: ""
  },
  customerEntity: {
    type: Object
  },
  isNotification: {
    type: Boolean,
    default: false
  },
  isPhoneVerified: {
    type: Boolean,
    default: false
  },
  isEmailVerified: {
    type: Boolean,
    default: false
  },
  isBlocked: {
    type: Boolean,
    default: false
  },
  isGuest: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: false
  },
  socketId: {
    type: String,
    default: ""
  },
  isSocialLogin: {
    type: Boolean,
    default: false
  },
  appleId: {
    type: String,
    default: "",
    index: true
  },
  googleId: {
    type: String,
    default: "",
    index: true
  },
  facebookId: {
    type: String,
    default: "",
    index: true
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  jti: {
    type: String,
    default: "",
    select: false,
    index: true
  },
  password: {
    type: String,
    default: ""
  },
  guestToken: {
    type: String,
    default: ""
  },
  referedBy: {
    type: String,
    default: ""
  },
  referalCode: {
    type: String,
    default: ""
  },
  deviceType: {
    type: String,
    default: "",
    enum: ["", "WEB", "ANDROID", "IOS"]
  },
  deviceToken: {
    type: String,
    default: "",
    index: true
  },
  loginCount: {
    type: Number,
    default: 0
  },
  wallet: {
    type: Number,
    default: 0
  },
  rating: {
    type: Number,
    default: 0
  }
}, {
  timestamps: true
});
userSchema.index({
  location: '2dsphere'
});


userSchema.methods.authenticate = function (password, callback) {
  const promise = new Promise((resolve, reject) => {
    if (!password) reject(new Error("MISSING_PASSWORD"));
    console.log("password", password);
    console.log("this password", this.password);
    bcrypt.compare(password, this.password, (error, result) => {
      if (!result) {
        reject(new Error("Invalid Password"));
      }
      resolve(this);
    });
  });

  if (typeof callback != "function") return promise;
  promise
    .then((result) => callback(null, result))
    .catch((err) => callback(err));
};

userSchema.methods.setPassword = async function (password) {
  let hashPwd = await bcrypt.hash(password, 10);
  this.password = hashPwd;
  return this;
};
userSchema.pre(/save|create|update/i, function (next) {
  if (this.get("latitude") && this.get("longitude")) {
    const longitude = this.get("longitude");
    const latitude = this.get("latitude");
    const location = {
      type: "Point",
      coordinates: [parseFloat(longitude), parseFloat(latitude)]
    };
    this.set({
      location
    });
  }
  next();
});

module.exports = mongoose.model('User', userSchema);