module.exports = {
    User: require("./User"),
    Driver: require("./Driver"),
    Otp: require("./Otp"),
    Admin: require("./Admin"),
    Vendor: require("./Vendor"),
    RequiredDocuments: require("./RequiredDocuments"),
    DriverDocuments: require("./DriverDocuments"),
    UserAddress: require("./UserAddress"),
    Cms: require("./Cms"),
    Notification: require("./Notification"),
    ChatMessage: require("./ChatMessage")
};