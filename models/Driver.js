const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-sequence')(mongoose);
const ObjectId = mongoose.Schema.Types.ObjectId;
const constants = require("../common/constants");

const DriverModel = new Schema({
    subscriptionId: {
        type: Schema.Types.ObjectId,
        ref: 'subscription',
        default: null
    },
    driverFor: {
        type: String,
        default: ""
    },
    friendReferralCode: {
        type: String,
        default: ""
    },
    age: {
        type: Number,
        default: 0
    },
    yearOfExp: {
        type: Number,
        default: 0
    },
    isSocialLogin: {
        type: Boolean,
        default: false
    },
    role: {
        type: String,
        enum: ["driver"],
        default: "driver"
    },
    loginCount: {
        type: Number,
        default: 0
    },
    cityId: {
        type: ObjectId,
        ref: "City",
        default: null
    },
    city: {
        type: String,
        default: ""
    },
    showPricing: {
        type: Number,
        enum: Object.values(constants.PRICING_TYPE)
    },
    salary: {
        type: Number,
        default: 0
    },
    country: {
        type: String
    },
    //not used
    name: {
        type: String,
        default: "",
        trim: true
    },
    firstName: {
        type: String,
        default: "",
        trim: true
    },
    lastName: {
        type: String,
        default: "",
        trim: true
    },
    email: {
        type: String,
        lowercase: true,
        trim: true
    },
    phone: {
        type: String,
        trim: true,
        default: ""
    },
    emergencyPhone: {
        type: String,
        trim: true,
        default: ""
    },
    emergencyCountryCode: {
      type: String,
      trim: true,
      default: ""
    },
    countryCode: {
        type: String,
        trim: true
    },
    countryISOCode: {
        type: String
    },
    isProfileComplete: {
        type: Boolean,
        default: false
    },
    isEmailVerify: {
        type: Boolean,
        default: false
    },
    isPhoneVerify: {
        type: Boolean,
        default: false
    },
    password: {
        type: String,
        default: ""
    },
    image: {
        type: String,
        default: ""
    },
    enabledZone: [{
        type: mongoose.Types.ObjectId
    } ],
    address: {
        type: String,
        default: ""
    },
    address2: {
        type: String,
        default: ""
    },
    isBlocked: {
        type: Boolean,
        default: false
    },
    isActive: {
      type: Boolean,
      default: false
    },
    socketId: {
        type: String
    },
    isBusy: {
        type: Boolean,
        default: false
    },
    isNotification: {
        type: Boolean,
        default: true
    },
    jti: {
        type: String
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    deviceType: {
        type: String,
        enum: ["", "IOS", "ANDROID", "WEB"]
    },
    verificationType: {
        type: Number,
        enum: [0, 1] //0 For Phone, 1 For email
    },
    deviceToken: {
        type: String,
        default: "",
        select: false
    },
    myReferralCode: {
        type: String,
        default: ""
    },
    totalTrips: {
        type: Number,
        default: 0
    },
    totalFleetTrips: {
        type: Number,
        default: 0
    },
    isDocumentUploaded: {
        type: Boolean,
        default: false
    },
    isDocumentVerified: {
        type: Boolean,
        default: false
    },
    isVehicleUpdated: {
        type: Boolean,
        default: false
    },
    isBankUpdated: {
        type: Boolean,
        default: false
    },
    bankDetails: {
        accountName: {
            type: String
        },
        bankName: {
            type: String
        },
        accountNo: {
            type: String
        },
        branchNo: {
            type: String
        }
    },
    vehicleDetails: {
        insurance: {
            type: String,
            default: ""
        },
        vehicleName: {
            type: String,
            default: ""
        },
        vehicleNumber: {
            type: String,
            default: ""
        },
        vehicleCategoryId: {
            type: Schema.Types.ObjectId,
            ref: "VehicleType"
        }
    },
    cashLimit: {
        type: Number,
        default: 0
    },
    cashInHand: {
        type: Number,
        default: 0
    },
    userLocation: {
        type: {
            type: String,
            enum: ["Point"],
            default: "Point"
        },
        coordinates: {
            type: [Number],
            default: [0, 0]
        }
    },
    rating: {
        type: Number,
        default: 0
    },
    ratingCount: {
        type: Number,
        default: 0
    },
    isSubscriptionActive: {
        type: Boolean,
        default: false
    },
    latitude: {
        type: Number,
        default: 0
    },
    longitude: {
        type: Number,
        default: 0
    },
    location: {
      type: {
        type: String,
        default: "Point"
      },
      coordinates: {
        type: [Number],
        default: [0, 0]
      }
    },
    locationUpdateTime: {
        type: Date,
        default: null
    },
    distanceDuration: {
        type: Number,
        default: 0
    },
    isOnline: {
        type: Boolean,
        default: false
    },
    isApproved: {
        type: Boolean,
        default: false
    },
    totalRating: {
        type: Number,
        default: 0
    },
    walletAmount: {
        type: Number,
        default: 0
    },
    dueBalance: {
        type: Number,
        default: 0
    },
    totalRatedBooking: {
        type: Number,
        default: 0
    },
    avgRating: {
        type: Number,
        default: 0
    },
    reviewTotal: {
        type: Number,
        default: 0
    },
    referralCode: {
        type: String
    },
    profileCompleteAt: {
        type: Number,
        default: 0
    },
    userRegions: [{
        type: String
    } ],
    b2bId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        default: null
    },
    assignedDriver: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        default: null
    },
    assignedTo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        default: null
    },
    parentCustomer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        default: null
    },
    assignedFromDate: {
        type: Date,
        default: ""
    },
    assignedToDate: {
        type: Date,
        default: ""
    },
    assignedFromTime: {
        type: String,
        default: ""
    },
    assignedToTime: {
        type: String,
        default: ""
    },
    driverNo: {
        type: Number,
        default: 0
    },
    blockUntil: {
        type: Date,
        default: new Date()
    },
    isTermViewed: {
        type: Boolean,
        default: false
    },
    showApis: {
        type: Boolean,
        default: false
    },
    companyName: {
        type: String,
        default: ""
    },
    lang: {
        type: String,
        default: "en"
    },
    isUpgraded: {
        type: Boolean,
        default: false
    },
    b2bServiceId: [{
        type: mongoose.Types.ObjectId,
        default: null
    }],
    b2bVehicleCategoryId: [{
        type: mongoose.Types.ObjectId,
        default: null
    } ],
    excludedPaymentMethod: [{
        type: Number
    }]
}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});
DriverModel.index({
    location: "2dsphere"
});
DriverModel.virtual("documents", {
    ref: "driverDocuments",
    localField: "_id",
    foreignField: "userId"
});

DriverModel.methods.authenticate = function (password, callback) { 

    const promise = new Promise((resolve, reject) => {
      if (!password) reject(new Error("MISSING_PASSWORD"));
      bcrypt.compare(password, this.password, (error, result) => {
        console.log(password,"password");
        console.log(this.password,"thispassword");
        if (!result) {
          reject(new Error("Invalid Password"));
        }
        resolve(this);
      });
    });
  
    if (typeof callback != "function") return promise;
    promise
      .then((result) => callback(null, result))
      .catch((err) => callback(err));
  };
  DriverModel.methods.setPassword = async function (password) {
    let hashPwd = await bcrypt.hash(password, 10);
    this.password = hashPwd;
    return this;
  };
  
DriverModel.plugin(autoIncrement, {inc_field: 'driverNo'});

const Driver = mongoose.model("Driver", DriverModel);
module.exports = Driver;