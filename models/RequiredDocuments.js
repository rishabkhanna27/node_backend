const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const constants = require("../common/constants");
const requiredDocumentModel = new Schema({
    name: {
        type: String,
        default:""
    },
    description: {
        type: String,
        default:""
    },
    require: {
        type: Boolean,
        default: false
    },
    bothSides: {
        type: Boolean,
        default: false
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    role: {
        type: Number,
        enum: [constants.ROLE.DRIVER, constants.ROLE.USER]
    }
}, {
    timestamps: true,
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});
const requiredDocument = mongoose.model('requiredDocument', requiredDocumentModel);
module.exports = requiredDocument;