module.exports = (io, socket) => {
  //Socket to connect stylist to its id.
  socket.on("onlineVendor", async function (data) {
    try {
      console.log("onlineVendor", data);
      if (data && data.vendorId) {
        console.log("connection vendor id:", data.vendorId);
        socket.join(data.vendorId);
        io.to(socket.id).emit("onlineVendorOk", {
          statusCode: 200,
          data: {},
          message: "Success",
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
    }
  });
};