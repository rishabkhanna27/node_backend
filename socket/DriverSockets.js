const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Model = require('../models/index');
const constants = require("../common/constants");
module.exports = (io, socket, socketData) => {

  socket.on("ordrStatusChange", async function (data) {
    try {
      console.log(data, "ordrStatusChange");
      if (data) {
        let order = await Model.Order.findOne({
            _id: ObjectId(data.id),
            $or: [{
              driverId: {
                $in: [socketData._id]
              }
            }, {
              requestedDriver: {
                $in: [socketData._id]
              }
            }],
            orderStatus: {
              $nin: [constants.ORDER_STATUS.CANCELLED, constants.ORDER_STATUS.COMPLETED]
            }
          }).populate("userId", "firstName lastName image email phone")
          .populate("packageTypeId").populate("promoCodeId")
          .populate("driverId", "firstName lastName image phone email rating");
        if (order == null) {
          throw new Error("Order not found!");
        }
        if (data.status == constants.ORDER_STATUS.COMPLETED) {
          throw new Error("Order not completed");
        } else if (order.orderStatus == constants.ORDER_STATUS.PENDING && data.status == constants.ORDER_STATUS.ACCEPTED) {
          let activeOrder = await Model.Order.findOne({
            orderStatus: {
              $nin: [constants.ORDER_STATUS.UNDELIVERED,constants.ORDER_STATUS.PENDING, constants.ORDER_STATUS.CANCELLED, constants.ORDER_STATUS.COMPLETED]
            },
            driverId: socketData._id
          });
          console.log(activeOrder, "activeOrder", socketData.isBusy, "socketData.isBusy");
          if (activeOrder != null || socketData.isBusy) {
            throw new Error("You already have an active order!");
          }
          order = await Model.Order.findOneAndUpdate({
              _id: order._id
            }, {
              $set: {
                driverId: socketData._id,
                orderStatus: constants.ORDER_STATUS.ACCEPTED
              }
            }, {
              new: true
            }).populate("userId", "firstName lastName image email phone")
            .populate("packageTypeId").populate("promoCodeId")
            .populate("driverId", "firstName lastName image phone email");
          await Model.Order.updateMany({
            driverId: {
              $ne: socketData._id
            },
            requestedDriver: socketData._id
          }, {
            $pull: {
              requestedDriver: socketData._id
            }
          });
          await Model.Driver.findOneAndUpdate({
            _id: socketData._id
          }, {
            $set: {
              isBusy: true
            }
          }, {
            new: true
          });
        } else if (order.orderStatus == constants.ORDER_STATUS.PENDING && data.status == constants.ORDER_STATUS.REJECTED) {
          order = await Model.Order.findOneAndUpdate({
              _id: order._id
            }, {
              $pull: {
                requestedDriver: socketData._id
              },
              $push: {
                cancelDrivers: socketData._id
              }
            }, {
              new: true
            }).populate("userId", "firstName lastName image email phone")
            .populate("packageTypeId").populate("promoCodeId")
            .populate("driverId", "firstName lastName image phone email");
        } else if (order.orderStatus != constants.ORDER_STATUS.PENDING) {
          order = await Model.Order.findOneAndUpdate({
              _id: order._id
            }, {
              $set: {
                orderStatus: data.status
              }
            }, {
              new: true
            }).populate("userId", "firstName lastName image email phone")
            .populate("packageTypeId").populate("promoCodeId")
            .populate("driverId", "firstName lastName image phone email");
        }
        let obj = {};
        if (order.orderStatus == constants.ORDER_STATUS.STARTED) {
          obj.pickUpStartedAt = new Date();
          process.emit("sendNotification", {
            userId: order.userId._id,
            receiverId: order.userId._id,
            orderId: order._id,
            values: order,
            role: constants.ROLE.USER,
            isNotificationSave: true,
            pushType: order.orderStatus
          });
        }
        if (order.orderStatus == constants.ORDER_STATUS.ACCEPTED) {
          obj.accepetdAt = new Date();
          process.emit("sendNotification", {
            userId: order.userId._id,
            receiverId: order.userId._id,
            orderId: order._id,
            values: order,
            role: constants.ROLE.USER,
            isNotificationSave: true,
            pushType: order.orderStatus
          });
        }
        if (order.orderStatus == constants.ORDER_STATUS.PICKED) {
          // if (!data.pickupProofImage) {
          //   throw new Error("Pickup proof image is required");
          // }
          // if (!data.pickupEsign) {
          //   throw new Error("Pickup Sign is required");
          // }
          // obj.pickupEsign = data.pickupEsign
          // obj.pickupProofImage = data.pickupProofImage
          obj.pickedAt = new Date();
          process.emit("sendNotification", {
            userId: order.userId._id,
            receiverId: order.userId._id,
            orderId: order._id,
            values: order,
            role: constants.ROLE.USER,
            isNotificationSave: true,
            pushType: order.orderStatus
          });
        }
        if (order.orderStatus == constants.ORDER_STATUS.ONTHEWAY) {
          obj.onthewayAt = new Date();
          process.emit("sendNotification", {
            userId: order.userId._id,
            receiverId: order.userId._id,
            orderId: order._id,
            values: order,
            role: constants.ROLE.USER,
            isNotificationSave: true,
            pushType: order.orderStatus
          });
        }
        let dataa = await Model.Order.findOneAndUpdate({
            _id: order._id
          }, {
            $set: obj
          }, {
            new: true
          }).populate("userId", "firstName lastName image email phone")
          .populate("packageTypeId").populate("promoCodeId")
          .populate("driverId", "firstName lastName image phone email rating");
        console.log(order.orderStatus, "3213213123213213");

        if (data.status) {
          let payload = {
            orderId: order._id
          };
          process.emit("orderStatusChange", payload);
        }
        io.to(dataa.userId._id).emit("ordrStatusChanged", {
          statusCode: 200,
          data: dataa,
          message: "Success",
          status: 1,
          isSessionExpired: false
        });
        io.to(socketData._id).emit("ordrStatusChanged", {
          statusCode: 200,
          data: dataa,
          message: "Success",
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
  //Socket to connect user to a specific room.
  socket.on("joinRoom", async function (data) {
    try {
      console.log("joinRoomdata", data);
      if (data && data.orderId != null) {
        let checkOrder = await Model.Order.findOne({
          _id: ObjectId(data.orderId)
        });
        if (checkOrder == null) {
          throw new Error("Order not found");
        }
        socket.join(data.orderId);
        await Model.Driver.findOneAndUpdate({
          _id: socketData._id
        }, {
          $set: {
            socketId: data.orderId
          }
        }, {
          new: true
        });
        let dataToSend = {
          isOnline: true
        };
        io.to(socketData._id).emit("joinRoomOk", {
          statusCode: 200,
          message: "Chat joined successfully",
          data: dataToSend,
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
  //Socket to leave user from a specific room.
  socket.on("leaveRoom", async function (data) {
    try {
      console.log("leaveRoomdata", data);
      if (data && data.orderId != null) {
        socket.leave(data.orderId);
        await Model.Driver.findOneAndUpdate({
          _id: socketData._id
        }, {
          $set: {
            socketId: ""
          }
        }, {
          new: true
        });
        let dataToSend = {
          isOnline: false
        };
        io.to(socketData._id).emit("leaveRoomOk", {
          statusCode: 200,
          message: "Chat leaved successfully",
          data: dataToSend,
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
  //Socket to send message to driver.
  socket.on('send_message_user', async (data) => {
    try {
      console.log("send_message_user", data);
      if (data.message && data.orderId) {
        let checkOrder = await Model.Order.findOne({
          _id: ObjectId(data.orderId)
        });
        if (checkOrder == null) {
          throw new Error("Order not found");
        }
        let obj = {
          userId: checkOrder.userId,
          driverId: checkOrder.driverId,
          senderId: checkOrder.driverId,
          messageType: data.messageType ? data.messageType : constants.MESSAGE_TYPE.MESSAGE,
          message: data.message,
          orderId: data.orderId,
          isDriverRead: true
        };
        let message = await Model.ChatMessage.create(obj);
        message = await Model.ChatMessage.findOne({
            _id: message._id
          }).populate("userId", "firstName lastName image")
          .populate("orderId", "orderStatus userId driverId orderNo")
          .populate("driverId", "firstName lastName image");

        let driverData = await Model.Driver.findOne({
          _id: checkOrder.driverId
        });
        if (driverData != null && JSON.stringify(driverData.socketId) != JSON.stringify(data.orderId)) {
          let payload = {
            id: driverData._id,
            orderId: message.orderId,
            message: data.message
          };
          process.emit("newMessage", payload);
          let payload1 = {
            userId: socketData._id,
            receiverId: socketData._id,
            orderId: message.orderId,
            values: data.message,
            role: constants.ROLE.USER,
            isNotificationSave: true,
            pushType: constants.PUSH_TYPE_KEYS.CHAT_MESSAGE
          };
          process.emit("sendNotification", payload1);
        }
        io.to(data.orderId).emit('send_message_Ok', {
          statusCode: 200,
          message: "New Message Received",
          data: message,
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
  //Socket to get the list of previous chats.
  socket.on("userList", async function (data) {
    try {
      console.log("userList", data);
      if (data) {
        let qry = {};
        if (data.search) {
          const regex = new RegExp(data.search, "i");
          qry._search = regex;
        }
        let chats = await Model.ChatMessage.aggregate([{
          $match: {
            driverId: socketData._id
          }
        }, {
          $group: {
            _id: "$orderId",
            orderId: {
              $last: "$orderId"
            },
            message: {
              $last: "$message"
            },
            userId: {
              $last: "$userId"
            },
            messageType: {
              $last: "$messageType"
            },
            driverId: {
              $last: "$driverId"
            },
            createdAt: {
              $last: "$createdAt"
            }
          }
        }, {
          "$lookup": {
            "from": "drivers",
            "let": {
              "driverId": "$driverId"
            },
            "pipeline": [{
                "$match": {
                  "$expr": {
                    "$eq": ["$_id", "$$driverId"]
                  }
                }
              },
              {
                "$project": {
                  "firstName": 1,
                  "lastName": 1,
                  "image": 1,
                  "phone": 1,
                  "email": 1
                }
              }
            ],
            "as": "driverId"
          }
        }, {
          "$lookup": {
            "from": "users",
            "let": {
              "userId": "$userId"
            },
            "pipeline": [{
                "$match": {
                  "$expr": {
                    "$eq": ["$_id", "$$userId"]
                  }
                }
              },
              {
                "$project": {
                  "firstName": 1,
                  "lastName": 1,
                  "image": 1,
                  "phone": 1,
                  "email": 1
                }
              }
            ],
            "as": "userId"
          }
        }, {
          "$lookup": {
            "from": "orders",
            "let": {
              "orderId": "$orderId"
            },
            "pipeline": [{
                "$match": {
                  "$expr": {
                    "$eq": ["$_id", "$$orderId"]
                  }
                }
              }
              // , {
              //   "$lookup": {
              //     "from": "reports",
              //     "let": {
              //       "userId": "$userId",
              //       "orderId": "$orderId"
              //     },
              //     "pipeline": [{
              //         "$match": {
              //           "$expr": {
              //             $and: [{
              //                 "$eq": ["$userId", "$$userId"]
              //               },
              //               {
              //                 "$eq": ["$orderId", "$$orderId"]
              //               }
              //             ]
              //           }
              //         }
              //       },
              //       {
              //         "$project": {
              //           "_id": 1
              //         }
              //       }
              //     ],
              //     "as": "reports"
              //   }
              // }, {
              //   "$addFields": {
              //     count: {
              //       $size: "$reports"
              //     }
              //   }
              // }, {
              //   "$project": {
              //     isReport: {
              //       $cond: {
              //         if: {
              //           $eq: ["$count", 0]
              //         },
              //         then: true,
              //         else: false
              //       }
              //     },
              //     "orderStatus": 1
              //   }
              // }
            ],
            "as": "orderId"
          }
        }, {
          "$unwind": "$orderId"
        }, {
          "$unwind": "$userId"
        }, {
          "$unwind": "$driverId"
        }, {
          "$lookup": {
            "from": "chatmessages",
            "let": {
              "driverId": "$driverId._id"
            },
            "pipeline": [{
              "$match": {
                "$expr": {
                  "$and": [{
                    "$eq": ["$driverId", "$$driverId"]
                  }, {
                    "$eq": ["$isVendorRead", false]
                  }]
                }
              }
            }, {
              $project: {
                _id: 1
              }
            }],
            "as": "count"
          }
        }, {
          $addFields: {
            pending: {
              $size: "$count"
            },
            _search: {
              $concat: ["$userId.firstName", "$userId.lastName"]
            }
          }
        }, {
          $match: qry
        }, {
          $sort: {
            createdAt: -1
          }
        }]);
        let dataToSend = {};
        dataToSend.chats = chats;
        io.to(socketData._id).emit("userListOk", {
          statusCode: 200,
          message: "Data Fetched",
          data: dataToSend,
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
  //Socket to update the location of driver while reaching the user.
  socket.on("updateLocation", async function (data) {
    try {
      console.log("updateLocation", data);
      if (data && data.orderId && data.longitude && data.latitude) {
        data.location = {
          type: "Point",
          coordinates: [Number(data.longitude), Number(data.latitude)]
        };
        let checkAddress = await Model.City.aggregate([{
          $match: {
            isDeleted: false,
            isBlocked: false,
            geoFence: {
              $geoIntersects: {
                $geometry: {
                  type: "Point",
                  coordinates: [parseFloat(data.longitude), parseFloat(data.latitude)]
                }
              }
            }
          }
        }]);
        await Model.Driver.findOneAndUpdate({
          _id: socketData._id
        }, {
          $set: {
            "latitude": data.latitude,
            "longitude": data.longitude,
            "location": data.location,
            "cityId": checkAddress[0] ? checkAddress[0]._id : socketData.cityId,
            "locationUpdateTime": new Date()
          }
        }, {
          new: true
        });
        let order = await Model.Order.findOne({
          _id: ObjectId(data.orderId)
        });
        io.to(order.userId).emit("updateLocationOk", {
          statusCode: 200,
          message: "Location updated successfully",
          data: order,
          status: 1,
          isSessionExpired: false
        });
        io.to(data.driverId).emit("updateLocationOk", {
          statusCode: 200,
          message: "Location updated successfully",
          data: {},
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
};