const Auth = require("../common/authenticate");

const admin = require("./AdminSockets");
const driver = require("./DriverSockets");
const user = require("./UserSockets");
const vendor = require("./VendorSockets");

const constants = require("../common/constants");
const Model = require('../models/index');
const pushNotificationService = require("../services/PushNotificationService");
const functions = require("../common/functions");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
let users = {};

module.exports = io => {
  io.use(async (socket, next) => {
    if (socket.handshake.query && socket.handshake.query.token) {
      let decoded = Auth.verifyToken(socket.handshake.query.token);
      if (!decoded) {
        return next(new Error("Authentication error"));
      } else {
        let role = decoded.role;
        if (decoded.role == 'user') {
          decoded = await Model.User.findOne({
            _id: decoded._id,
            isBlocked: false,
            isDeleted: false
          });
        } else if (decoded.role == 'driver') {
          decoded = await Model.Driver.findOne({
            _id: decoded._id,
            isBlocked: false,
            isDeleted: false
          });
        } else if (decoded.role == 'admin') {
          decoded = await Model.Admin.findOne({
            _id: decoded._id,
            isBlocked: false,
            isDeleted: false
          });
        } else if (decoded.role == 'vendor') {
          decoded = await Model.Vendor.findOne({
            _id: decoded._id,
            isBlocked: false,
            isDeleted: false
          });
        }
        if (!decoded || decoded.isDeleted || decoded.isBlocked) {
          next(new Error("Authentication error"));
          return;
        }
        decoded.role = role;
        console.log("Socket role", decoded.role);
        console.log("Connect User", decoded.phone, socket.handshake.query.token);
        users[String(socket.id)] = decoded;

        socket.join(String(decoded._id));
        socket.emit("connected", {
          status: true
        });
        next();
      }
    } else {
      console.log("unauth connected");
      next();
    }
  }).on("connection", async function (socket) {
    let socketData = users[String(socket.id)];
    console.log("USER CONNECTED", socket.id, "socket id", socketData.phone, "role", socketData.role);

    if (socketData.role == "user" || socketData.role == constants.ROLE.USER) {
      user(io, socket, socketData);
    }
    if (socketData.role == "driver" || socketData.role == constants.ROLE.DRIVER) {
      driver(io, socket, socketData);
    }
    if (socketData.role == "admin" || socketData.role == constants.ROLE.ADMIN || socketData.role == constants.ROLE.SUBADMIN) {
      socket.join("newOrder");
      socket.join("orderStatus");
      admin(io, socket, socketData);
    }
    if (socketData.role == "vendor" || socketData.role == constants.ROLE.VENDOR) {
      socket.join("newOrder");
      socket.join("orderStatus");
      vendor(io, socket, socketData);
    }
    socket.on("disconnect", async function () {
      console.log("Disconnect", socket.id, "User Phone", socketData.phone);
      console.log(socket.rooms, "Rooms joined by socketId", socket.id);
      if (socketData.role == constants.ROLE.DRIVER) {
        await Model.Driver.updateOne({
          _id: socketData._id
        }, {
          $set: {
            isOnline: false
          }
        });
      }
    });
  });
  //Process for sending notification to user.
  process.on("sendNotification", async function (payloadData) {
    console.log("sendNotification");
    try {
      if (payloadData && payloadData.receiverId) {
        payloadData.pushType = payloadData.pushType ? payloadData.pushType : 0;
        let lang = payloadData.lang || "en";
        let role = payloadData.role || constants.ROLE.USER;
        let notificationType = payloadData.notificationType || constants.NOTIFICATION_TYPE.PUSH;
        let values = payloadData.values ? payloadData.values : {};
        let inputKeysObj = constants.PUSH_TYPE[payloadData.pushType];
        let data = await functions.renderTemplateField(
          inputKeysObj,
          values,
          lang,
          payloadData
        );
        io.to(payloadData.receiverId).emit('sendNotificationOk', data);
        if (notificationType == constants.NOTIFICATION_TYPE.PUSH) {
          pushNotificationService.preparePushNotifiction(data, role, notificationType);
        } else if (notificationType == constants.NOTIFICATION_TYPE.BROADCAST) {
          pushNotificationService.broadcastNotifications(payloadData, role);
        }
      }

    } catch (err) {
      console.log(err);
    }
  });
  //New Order
  process.on("newOrder", async function (payload) {
    let data = io.sockets.adapter.rooms['newOrder'];
    console.log(data, "sockets in newOrder");
    io.to("newOrder").emit('newOrderOk', {
      statusCode: 200,
      message: "Success",
      data: payload,
      status: 1,
      isSessionExpired: false
    });
  });
  //New Order
  process.on("clearStack", async function (payload) {
    io.to(payload.driverId).emit('clearStackOk', {
      statusCode: 200,
      message: "Success",
      data: {},
      status: 1,
      isSessionExpired: false
    });
  });
  //Notify Driver
  process.on("newRequest", async function (payload) {
    io.to(payload.id).emit('newRequestOk', {
      statusCode: 200,
      message: "Success",
      data: payload,
      status: 1,
      isSessionExpired: false
    });
  });
  //Order status
  process.on("ordrStatusChange", async function (payload) {
    console.log(payload, "ordrStatusChange");
    let order = await Model.Order.findOne({
        _id: payload.orderId
      }).populate("userId", "firstName lastName image email phone")
      .populate("packageTypeId").populate("promoCodeId")
      .populate("driverId", "firstName lastName image phone email");
    if (order == null) {
      throw new Error("Order not found!");
    }
    io.to("orderStatus").emit("ordrStatusChanged", {
      statusCode: 200,
      data: order,
      message: "Order status changed.",
      status: 1,
      isSessionExpired: false
    });
    io.to(payload.userId).emit("ordrStatusChanged", {
      statusCode: 200,
      data: order,
      message: "Order status changed.",
      status: 1,
      isSessionExpired: false
    });
    io.to(payload.driverId).emit("ordrStatusChanged", {
      statusCode: 200,
      data: order,
      message: "Order status changed.",
      status: 1,
      isSessionExpired: false
    });
  });
  //Pending notification
  process.on("getNotification", async (data) => {
    let notificationCount;
    if (data.userId != null && data.userId != "") {
      notificationCount = await Model.Notification.countDocuments({
        receiverId: ObjectId(data.userId),
        role: constants.ROLE.USER,
        isDeleted: false,
        isUserRead: false
      });
      if (notificationCount == null) {
        notificationCount = 0;
      }
      io.to(data.userId).emit("notificationCount", {
        status: 200,
        message: "Notification Count",
        data: notificationCount
      });
    }
    if (data.driverId != null && data.driverId != "") {
      notificationCount = await Model.Notification.countDocuments({
        receiverId: ObjectId(data.driverId),
        role: constants.ROLE.DRIVER,
        isDeleted: false,
        isVendorRead: false
      });
      if (notificationCount == null) {
        notificationCount = 0;
      }
      io.to(data.driverId).emit("notificationCount", {
        status: 200,
        message: "Notification Count",
        data: notificationCount
      });
    }
  });
};