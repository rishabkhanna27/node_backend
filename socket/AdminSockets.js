module.exports = (io, socket) => {
  //Socket to connect admin to its id.
  socket.on("onlineAdmin", async function (data) {
    try {
      console.log("onlineAdmin", data);
      if (data && data.adminId) {
        console.log("connection vendor id:", data.adminId);
        socket.join(data.adminId);
        io.to(socket.id).emit("onlineAdminOk", {
          statusCode: 200,
          data: {},
          message: "Success",
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
    }
  });
};