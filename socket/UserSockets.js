const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Model = require('../models/index');
const constants = require("../common/constants");

module.exports = (io, socket, socketData) => {
  //Socket to connect user to a specific room.
  socket.on("joinRoom", async function (data) {
    try {
      console.log("joinRoomdata", data);
      if (data && data.orderId != null) {
        let checkOrder = await Model.Order.findOne({
          _id: ObjectId(data.orderId)
        });
        if (checkOrder == null) {
          throw new Error("Order not found");
        }
        socket.join(data.orderId);
        await Model.User.findOneAndUpdate({
          _id: socketData._id
        }, {
          $set: {
            socketId: data.orderId
          }
        }, {
          new: true
        });
        let dataToSend = {
          isOnline: true
        };
        io.to(socketData._id).emit("joinRoomOk", {
          statusCode: 200,
          message: "Chat joined successfully",
          data: dataToSend,
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
  //Socket to leave user from a specific room.
  socket.on("leaveRoom", async function (data) {
    try {
      console.log("leaveRoomdata", data);
      if (data && data.orderId != null) {
        socket.leave(data.orderId);
        await Model.User.findOneAndUpdate({
          _id: socketData._id
        }, {
          $set: {
            socketId: ""
          }
        }, {
          new: true
        });
        let dataToSend = {
          isOnline: false
        };
        io.to(socketData._id).emit("leaveRoomOk", {
          statusCode: 200,
          message: "Chat leaved successfully",
          data: dataToSend,
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
  //Socket to send message to stylist.
  socket.on('send_message_user', async (data) => {
    try {
      console.log("send_message_user", data);
      if (data.message && data.orderId) {
        let checkOrder = await Model.Order.findOne({
          _id: ObjectId(data.orderId)
        });
        if (checkOrder == null) {
          throw new Error("Order not found");
        }
        let obj = {
          userId: socketData._id,
          driverId: checkOrder.driverId,
          senderId: socketData._id,
          messageType: data.messageType ? data.messageType : constants.MESSAGE_TYPE.MESSAGE,
          message: data.message,
          orderId: data.orderId,
          isUserRead: true
        };
        let message = await Model.ChatMessage.create(obj);
        message = await Model.ChatMessage.findOne({
          _id: message._id
        }).populate("userId", "firstName lastName image")
        .populate("orderId", "orderStatus userId driverId orderNo")
        .populate("driverId", "firstName lastName image");

        let driverData = await Model.Driver.findOne({
          _id: checkOrder.driverId
        });
        if (driverData != null && JSON.stringify(driverData.socketId) != JSON.stringify(data.orderId)) {
          let payload = {
            id: driverData._id,
            orderId: message.orderId,
            message: data.message
          };
          process.emit("newMessage", payload);
          let payload1 = {
            receiverId: driverData._id,
            driverId: driverData._id,
            orderId: message.orderId,
            values: data.message,
            role: constants.ROLE.DRIVER,
            isNotificationSave: true,
            pushType: constants.PUSH_TYPE_KEYS.CHAT_MESSAGE
          };
          process.emit("sendNotification", payload1);
        }
        console.log(message,"message");
        io.to(data.orderId).emit('send_message_Ok', {
          statusCode: 200,
          message: "New Message Received",
          data: message,
          status: 1,
          isSessionExpired: false
        });
      }
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
  //Socket to get the list of previous chats.
  socket.on("userList", async function (data) {
    try {
      console.log("userList", data);
      let qry = {};
      if (data.search) {
        const regex = new RegExp(data.search, "i");
        qry._search = regex;
      }
      let chats = await Model.ChatMessage.aggregate([{
          $match: {
            userId: socketData._id
          }
        }, {
          $group: {
            _id: "$orderId",
            orderId: {
              $last: "$orderId"
            },
            message: {
              $last: "$message"
            },
            userId: {
              $last: "$userId"
            },
            messageType: {
              $last: "$messageType"
            },
            driverId: {
              $last: "$driverId"
            },
            createdAt: {
              $last: "$createdAt"
            }
          }
        }, {
          "$lookup": {
            "from": "drivers",
            "let": {
              "driverId": "$driverId"
            },
            "pipeline": [{
                "$match": {
                  "$expr": {
                    "$eq": ["$_id", "$$driverId"]
                  }
                }
              },
              {
                "$project": {
                  "firstName": 1,
                  "lastName": 1,
                  "image": 1,
                  "phone": 1,
                  "email": 1
                }
              }
            ],
            "as": "driverId"
          }
        }, {
          "$lookup": {
            "from": "users",
            "let": {
              "userId": "$userId"
            },
            "pipeline": [{
                "$match": {
                  "$expr": {
                    "$eq": ["$_id", "$$userId"]
                  }
                }
              },
              {
                "$project": {
                  "firstName": 1,
                  "lastName": 1,
                  "image": 1,
                  "phone": 1,
                  "email": 1
                }
              }
            ],
            "as": "userId"
          }
        }, {
          "$lookup": {
            "from": "orders",
            "let": {
              "orderId": "$orderId"
            },
            "pipeline": [{
              "$match": {
                "$expr": {
                  "$eq": ["$_id", "$$orderId"]
                }
              }
            }
            // , {
            //   "$lookup": {
            //     "from": "reports",
            //     "let": {
            //       "userId": "$userId",
            //       "orderId": "$orderId"
            //     },
            //     "pipeline": [{
            //         "$match": {
            //           "$expr": {
            //             $and: [{
            //                 "$eq": ["$userId", "$$userId"]
            //               },
            //               {
            //                 "$eq": ["$orderId", "$$orderId"]
            //               }
            //             ]
            //           }
            //         }
            //       },
            //       {
            //         "$project": {
            //           "_id": 1
            //         }
            //       }
            //     ],
            //     "as": "reports"
            //   }
            // }, {
            //   "$addFields": {
            //     count: {
            //       $size: "$reports"
            //     }
            //   }
            // }, {
            //   "$project": {
            //     isReport: {
            //       $cond: {
            //         if: {
            //           $eq: ["$count", 0]
            //         },
            //         then: true,
            //         else: false
            //       }
            //     },
            //     "orderStatus": 1
            //   }
            // }
          ],
            "as": "orderId"
          }
        }, {
          "$unwind": "$orderId"
        }, {
          "$unwind": "$userId"
        }, {
          "$unwind": "$driverId"
        }, {
          "$lookup": {
            "from": "chatmessages",
            "let": {
              "driverId": "$driverId._id"
            },
            "pipeline": [{
              "$match": {
                "$expr": {
                  "$and": [{
                    "$eq": ["$driverId", "$$driverId"]
                  }, {
                    "$eq": ["$isUserRead", false]
                  }]
                }
              }
            }, {
              $project: {
                _id: 1
              }
            }],
            "as": "count"
          }
        }, {
          $addFields: {
            pending: {
              $size: "$count"
            },
            _search: {
              $concat: ["$driverId.firstName", "$driverId.lastName"]
            }
          }
        }, {
          $match: qry
        }, {
          $sort: {
            createdAt: -1
          }
        }
      ]);
      let dataToSend = {};
      dataToSend.chats = chats;
      io.to(socketData._id).emit("userListOk", {
        statusCode: 200,
        message: "Data Fetched",
        data: dataToSend,
        status: 1,
        isSessionExpired: false
      });
    } catch (error) {
      console.error(error.message || error);
      io.to(socketData._id).emit("errorSocket", {
        statusCode: 400,
        message: error.message || error,
        data: {},
        status: 1,
        isSessionExpired: false
      });
    }
  });
};