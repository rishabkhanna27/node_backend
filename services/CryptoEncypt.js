//Using Crypto
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const ivKey = '7bcy5tyvctfatrtv';

//Using CryptoJs
const CryptoJS = require("crypto-js");
var iv = CryptoJS.enc.Base64.parse("7bcy5tyvctfatrtv"); //giving empty initialization vector
var key = CryptoJS.SHA256("2ka7xvzegahga34"); //hashing the key using SHA256

const moment = require("moment");

// module.exports.secKeyEncryptWithAppKey = async (payload) => {
//     try {
//         let appkey = payload.appkey;
//         const key = crypto.randomBytes(32);
//         console.log('key', key);
//         let cipher = crypto.createCipheriv(algorithm, Buffer.from(key, 'base64'), ivKey);
//         let encrypted = cipher.update(appkey);
//         encrypted = Buffer.concat([encrypted, cipher.final()]);
//         let encryptedData = encrypted.toString('hex');
//         console.log('sek', encryptedData);
//         console.log('key hash', key.toString('hex'));
//         console.log('appkey', appkey);
//         return {
//             iv: ivKey.toString('hex'),
//             sek: encrypted.toString('hex'),
//             hash: key.toString('hex')
//         };

//     } catch (error) {
//         console.log('', error);
//     }
// };
// module.exports.secKeyDecryptedWithSek = async (req, res, next) => {
//     if (!req.headers.hash || !req.headers.sek) {
//         return res.status(429).send({
//             "statusCode": 429,
//             "message": "BLOCKED ACCESS",
//             "data": {},
//             "status": 0,
//             "isSessionExpired": true
//         });
//     }
//     let hash = req.headers.hash;
//     let sek = req.headers.sek;
//     let key = Buffer.from(sek, 'hex');
//     hash = Buffer.from(hash, 'hex');
//     let decipher = crypto.createDecipheriv(algorithm, Buffer.from(hash, 'base64'), ivKey);
//     let decrypted = decipher.update(key);
//     decrypted = Buffer.concat([decrypted, decipher.final()]);
//     decrypted = Number(decrypted.toString());
//     if (moment(decrypted) == "Invalid date" || moment().diff(moment(decrypted), "s") >= 60) {
//         return res.status(429).send({
//             "statusCode": 429,
//             "message": "BLOCKED ACCESS",
//             "data": {},
//             "status": 0,
//             "isSessionExpired": true
//         });
//     }
//     next();
// };



// module.exports.encryptData = async (data) => {
//     let encryptedString;
//     if (typeof data == "string") {
//         data = data.slice();
//         encryptedString = CryptoJS.AES.encrypt(data, key, {
//             iv: iv,
//             mode: CryptoJS.mode.CBC
//         });
//     } else {
//         encryptedString = CryptoJS.AES.encrypt(JSON.stringify(data), key, {
//             iv: iv,
//             mode: CryptoJS.mode.CBC
//         });
//     }
//     return encryptedString.toString();
// };
// module.exports.decryptData = async (encrypted) => {
//     let decrypted = CryptoJS.AES.decrypt(encrypted, key, {
//         iv: iv,
//         mode: CryptoJS.mode.CBC
//     });
//     return decrypted.toString(CryptoJS.enc.Utf8);
// };


module.exports.encyptInput = async (data) => {
    if ((data.deviceType).toLowerCase() == "web") {
        let encryptedString;
        if (typeof data.payload == "string") {
            data.payload = data.payload.slice();
            encryptedString = CryptoJS.AES.encrypt(data.payload, key, {
                iv: iv,
                mode: CryptoJS.mode.CBC
            });
        } else {
            encryptedString = CryptoJS.AES.encrypt(JSON.stringify(data), key, {
                iv: iv,
                mode: CryptoJS.mode.CBC
            });
        }
        return {
            iv: iv.toString('hex'),
            hash: encryptedString.toString('hex')
        };
    } else {
        let appkey = data.appkey;
        const key = crypto.randomBytes(32);
        let cipher = crypto.createCipheriv(algorithm, Buffer.from(key, 'base64'), ivKey);
        let encrypted = cipher.update(appkey);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        let encryptedData = encrypted.toString('hex');
        console.log('sek', encryptedData);
        console.log('key hash', key.toString('hex'));
        console.log('appkey', appkey);
        return {
            iv: ivKey.toString('hex'),
            sek: encrypted.toString('hex'),
            hash: key.toString('hex')
        };
    }
};
module.exports.decryptInput = async (req, res, next) => {
    if ((req.headers.devicetype).toLowerCase() == "web") {
        if (!req.headers.hash) {
            return res.status(429).send({
                "statusCode": 429,
                "message": "BLOCKED ACCESS",
                "data": {},
                "status": 0,
                "isSessionExpired": true
            });
        }
        let hash = req.headers.hash;
        hash = Buffer.from(hash, 'hex');
        let decrypted = CryptoJS.AES.decrypt(hash, key, {
            iv: iv,
            mode: CryptoJS.mode.CBC
        });
        decrypted = Number(decrypted.toString());
        if (moment(decrypted) == "Invalid date" || moment().diff(moment(decrypted), "s") >= 10) {
            return res.status(429).send({
                "statusCode": 429,
                "message": "BLOCKED ACCESS",
                "data": {},
                "status": 0,
                "isSessionExpired": true
            });
        }
        next();
    } else {
        if (!req.headers.hash || !req.headers.sek) {
            return res.status(429).send({
                "statusCode": 429,
                "message": "BLOCKED ACCESS",
                "data": {},
                "status": 0,
                "isSessionExpired": true
            });
        }
        let hash = req.headers.hash;
        let sek = req.headers.sek;
        let key = Buffer.from(sek, 'hex');
        hash = Buffer.from(hash, 'hex');
        let decipher = crypto.createDecipheriv(algorithm, Buffer.from(hash, 'base64'), ivKey);
        let decrypted = decipher.update(key);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        decrypted = Number(decrypted.toString());
        if (moment(decrypted) == "Invalid date" || moment().diff(moment(decrypted), "s") >= 10) {
            return res.status(429).send({
                "statusCode": 429,
                "message": "BLOCKED ACCESS",
                "data": {},
                "status": 0,
                "isSessionExpired": true
            });
        }
        next();
    }
};