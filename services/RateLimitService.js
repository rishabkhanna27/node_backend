const rateLimit = require("express-rate-limit");

module.exports.signupForgotLimiter = rateLimit({
    windowMs: 30000, // 30 second in milliseconds
    max: 5, // limit each IP to 3 requests per second
    message: {
        "statusCode": 429,
        "message": "Forbidden",
        "data": {},
        "status": 0,
        "isSessionExpired": false
    },
    headers: true,
    keyGenerator: function (req) {
        return req.ip;
    },
    handler: function (req, res) {
        return res.status(429).send({
            statusCode: 429,
            message: "Too many requests, please try again later.",
            data: {},
            status: 0,
            "isSessionExpired": true
        });
    },
    onLimitReached: function (req, res) {
        const retryAfter = Math.ceil(300000 / 1000); // calculate retry time in seconds
        res.set("Retry-After", String(retryAfter)); // set Retry-After header in response
    }
});

module.exports.globalLimiter = rateLimit({
    windowMs: 10000, // 1 Minute in milliseconds
    max: 10, // limit each IP to 10 requests per Minute
    message: {
        "statusCode": 429,
        "message": "Forbidden",
        "data": {},
        "status": 0,
        "isSessionExpired": false
    },
    headers: true,
    keyGenerator: function (req) {
        return req.ip;
    },
    handler: function (req, res) {
        return res.status(429).send({
            statusCode: 429,
            message: "Too many requests, please try again later.",
            data: {},
            status: 0,
            "isSessionExpired": true
        });
    },
    onLimitReached: function (req, res) {
        const retryAfter = Math.ceil(10000 / 1000); // calculate retry time in seconds
        res.set("Retry-After", String(retryAfter)); // set Retry-After header in response
    }
});