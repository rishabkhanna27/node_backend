// const twilio = require("twilio");
// const mongoose = require("mongoose");
// const ObjectId = mongoose.Types.ObjectId;
// let accountSid = process.env.TWILIO_SID; 
// let authToken = process.env.TWILIO_AUTH;
// const client = twilio(accountSid, authToken, {
//   lazyLoading: true
// });
// const fromNumber = process.env.FROM_NUMBER;
const constants = require('../common/constants');
const Model = require('../models/index');

// const sendSMSTwillo = async ({countryCode, phone, message, id}) => {

//     return new Promise((resolve) => {
//         countryCode = countryCode.replace("+","");
//         countryCode = "+" + countryCode;
//         console.log(countryCode,"countryCode");
//         console.log(phone,"phone");
//         const smsOptions = {
//             from: fromNumber,
//             to: countryCode + (phone ? phone.toString() : ""),
//             body: message
//         };
//         client.messages.create(smsOptions)
//         .then(async (message) => {
//             console.log(message.sid);
//             await Model.Otp.updateOne({
//                 _id: ObjectId(id)
//             },{
//                 $set: {sid: message.sid}
//             });
//             resolve(message.sid);
//         }).catch(err => {
//             console.log(err);
//         });
//     });
// };
const sendSMS = async (payload) => {
    return new Promise((resolve) => {
        console.log("sms send ", payload);
        // sendSMSTwillo(payload);
        return resolve(payload.message);
    });
};

//Send Verification otp. 
exports.sendPhoneVerification = async (payload) => {
    try {
        console.log(payload);
        if (!payload.countryCode) throw new Error(constants.MESSAGES.DIAL_CODE_MISSING);
        if (!payload.phone) throw new Error(constants.MESSAGES.PHONE_MISSING);
        // payload.otp = functions.generateNumber(4);
        payload.otp = 1234;
        console.log(payload);
        await Model.Otp.deleteMany({
            countryCode : payload.countryCode,
            phone : payload.phone
        });
        let id = await Model.Otp.create(payload);
        let payloadData = {
            phone: payload.phone,
            countryCode: payload.countryCode,
            message: `Your verification code is ${payload.otp}`,
            id: id._id
        };
        console.log("OTP----------------->",payloadData.message,"<------------------");
        await sendSMS(payloadData);
        return true;
    } catch (error) {
        console.error("sendPhoneVerification", error);
    }
};
exports.sendOrderOtp = async (payload) => {
    try {
        console.log(payload);
        if (!payload.countryCode) throw new Error(constants.MESSAGES.DIAL_CODE_MISSING);
        if (!payload.phone) throw new Error(constants.MESSAGES.PHONE_MISSING);
        // payload.otp = functions.generateNumber(4);
        payload.otp = 1234;
        console.log(payload);
        await Model.Otp.deleteMany({
            orderId: payload.orderId
        });
        await Model.Otp.create(payload);
        let payloadData = {
            phone: payload.phone,
            countryCode: payload.countryCode,
            message: `Your verification code is ${payload.otp}`
        };
        console.log("OTP----------------->",payloadData.message,"<------------------");
        await sendSMS(payloadData.countryCode, payloadData.phone, payloadData.message);
        return true;
    } catch (error) {
        console.error("sendOrderOtp", error);
    }
};