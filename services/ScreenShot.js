const puppeteer = require("puppeteer");
const Model = require('../models/index');
// const services= require("./index");
const aws = require("aws-sdk");
var fs = require('fs');

aws.config.update({
    secretAccessKey: process.env.AWS_SECRET,
    accessKeyId: process.env.AWS_KEY
});
var s3 = new aws.S3();


module.exports.screenShot = async (payload) => {
    puppeteer
        .launch({
            args: ['--no-sandbox']
        })
        .then(async (browser) => {
            const page = await browser.newPage();
            await page.goto(payload.url, {
                waitUntil: 'networkidle2'
            });
            await page.waitForSelector('#invoice', {
                visible: true
            });
            await page.addStyleTag({
                content: '@page { size: auto; }'
            });
            await page.pdf({
                path: "public/" + payload.random + ".pdf",
                preferCSSPageSize: true,
                printBackground: true
            });
            await browser.close();
            console.log("Done");
            let fileData = fs.readFileSync(`public/${payload.random}.pdf`);
            const params = {
                bucket: process.env.AWS_BUCKET,
                Key: `${payload.random}.pdf`,
                Body: fileData
            };
            s3.upload(params, async function (err, data) {
                if (err) {
                    throw err;
                } else {
                    setTimeout(async () => {
                        await Model.TransactionModel.findOneAndUpdate({
                            paymentId: payload.transactionId
                        }, {
                            $set: {
                                "invoice": data.Location
                            }
                        }, {
                            new: true
                        });
                        fileData = fileData.toString("base64");
                        payload.fileData = fileData;
                        payload.file = `public/${payload.random}.pdf`;
                        // services.EmalService.sendReceipt(payload);

                    }, 400);
                }
            });
            return true;
        }).catch(error => {
            console.log("Error", error);
            return false;
        });
};