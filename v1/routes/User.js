const Controller = require('../controller');
const Auth = require("../../common/authenticate");
const router = require('express').Router();

//Onboarding
router.post("/signup", Controller.UserController.signup);
router.post("/socialLogin", Controller.UserController.socialLogin);
router.post("/login", Controller.UserController.login);
router.get("/logout", Auth.verify("User"), Controller.UserController.logout);
router.get("/getProfile", Auth.verify("User"), Controller.UserController.getProfile);
router.delete("/deleteProfile", Auth.verify("User"), Controller.UserController.deleteProfile);
router.put("/updateProfile", Auth.verify("User"), Controller.UserController.updateProfile);
router.post("/resetPassword", Auth.verify("User"), Controller.UserController.resetPassword);
router.put("/changePassword", Auth.verify("User"), Controller.UserController.changePassword);
router.post("/forgotPassword", Controller.UserController.sendOtp);

//Otp
router.post("/sendOtp", Controller.UserController.sendOtp);
router.post("/verifyOtp", Controller.UserController.verifyOtp);

//Address
router.post("/addUserAddress", Auth.verify("User"), Controller.UserController.addUserAddress);
router.post("/getAddress/:id?", Auth.verify("User"), Controller.UserController.getAddress);
router.post("/updatedAddress/:id", Auth.verify("User"), Controller.UserController.updatedAddress);
router.delete("/deleteAddress/:id", Auth.verify("User"), Controller.UserController.deleteAddress);

//CMS
router.get("/getCms", Controller.UserController.getCms);
router.get("/test", Controller.UserController.test);

module.exports = router;