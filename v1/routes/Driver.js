const Controller = require('../controller');
const Auth = require("../../common/authenticate");
const router = require('express').Router();

// ONBOARDING
router.post("/signupOtp", Controller.DriverController.signupOtp);
router.post("/signup", Controller.DriverController.createUser);
router.post("/login", Controller.DriverController.login);
router.post("/updateProfile", Auth.verify("Driver"), Controller.DriverController.updateProfile);
router.post("/logout", Auth.verify("Driver"), Controller.DriverController.logout);
router.post("/forgotPassword", Controller.DriverController.forgotPassword);
router.post("/verifyOtp", Controller.DriverController.verifyOtp);
router.post("/setPassword", Auth.verify("Driver"), Controller.DriverController.resetPassword);
router.post("/changePassword", Auth.verify("Driver"), Controller.DriverController.changePassword);
router.get("/getProfile", Auth.verify("Driver"), Controller.DriverController.getProfile);

//Documents
router.get("/requiredDocument", Controller.DriverController.requiredDocument);
router.get("/getDriverDocuments", Auth.verify("Driver"), Controller.DriverController.getDriverDocuments);
router.post("/uploadDocuments", Auth.verify("Driver"), Controller.DriverController.uploadDocuments);

module.exports = router;