const router = require("express").Router();
const UsersRoutes = require("./User");
const VendorRoutes = require("./Vendor");
const AdminRoutes = require("./Admin");
const DriverRoutes = require("./Driver");
const uploadRoutes=require("./Upload");

router.use("/Admin", AdminRoutes);
router.use("/Driver", DriverRoutes);
router.use("/User", UsersRoutes);
router.use("/Vendor", VendorRoutes);
router.use("/upload",uploadRoutes);

module.exports = router;