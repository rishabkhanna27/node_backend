const router = require("express").Router();
const Auth = require("../../common/authenticate");
const Controller = require("../controller");

// ONBOARDING API'S
router.post("/register", Controller.AdminController.register);
router.post("/login", Controller.AdminController.login);
router.get("/logout", Auth.verify("admin"), Controller.AdminController.logout);
router.get("/getProfile", Auth.verify("admin"), Controller.AdminController.getProfile);
router.put("/updateProfile", Auth.verify("admin"), Controller.AdminController.updateProfile);
router.post("/changePassword", Auth.verify("admin"), Controller.AdminController.changePassword);
router.post("/resetPassword", Controller.AdminController.resetPassword);
router.post("/forgotPassword", Controller.AdminController.forgotPassword);

// CMS
router.get("/getCms", Controller.AdminController.getCms);
router.post("/addCms", Auth.verify("admin"), Controller.AdminController.addCms);

module.exports = router;