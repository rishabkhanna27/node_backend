const router = require("express").Router();
const Auth = require("../../common/authenticate");
const Controller = require("../controller");

// ONBOARDING API'S
router.post("/register", Controller.VendorController.register);
router.post("/login", Controller.VendorController.login);
router.get("/logout", Auth.verify("vendor"), Controller.VendorController.logout);
router.get("/getProfile", Auth.verify("vendor"), Controller.VendorController.getProfile);
router.put("/updateProfile", Auth.verify("vendor"), Controller.VendorController.updateProfile);
router.post("/changePassword", Auth.verify("vendor"), Controller.VendorController.changePassword);
router.post("/resetPassword", Controller.VendorController.resetPassword);
router.post("/forgotPassword", Controller.VendorController.forgotPassword);

module.exports = router;