module.exports = {
    Admin: require("./Admin"),
    Driver: require("./Driver"),
    User: require("./User"),
    Vendor: require("./Vendor"),
    Global: require("./Global")
};