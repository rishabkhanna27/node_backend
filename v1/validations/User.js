const Joi = require("joi").defaults((schema) => {
    switch (schema.type) {
        case "string":
            return schema.replace(/\s+/, " ");
        default:
            return schema;
    }
});

Joi.objectId = () => Joi.string().pattern(/^[0-9a-f]{24}$/, "valid ObjectId");

module.exports.identify = Joi.object({
    id: Joi.objectId().required()
});

module.exports.signup = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().required().error(new Error("Please Enter a valid email")),
    name: Joi.string().required(),
    password: Joi.string().optional(),
    confirmPassword: Joi.ref("password"),
    deviceToken: Joi.string().optional(),
    deviceType: Joi.string().optional()
});

module.exports.validateSendOtp = Joi.object({
        key: Joi.string().required(),
        role: Joi.string().optional(),
        email: Joi.string().optional(),
        hash: Joi.string().optional(),
        sek: Joi.string().optional(),
        appKey: Joi.string().optional(),
        countryCode: Joi.string().allow("", null).optional(),
        password: Joi.string().allow("", null).optional()
});

module.exports.socialLogin = Joi.object({
    appleId: Joi.string().optional().allow(""),
    googleId: Joi.string().optional().allow(""),
    facebookId: Joi.string().optional().allow(""),
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    name: Joi.string().optional(),
    deviceToken: Joi.string().optional(),
    deviceType: Joi.string().optional()
});

module.exports.login = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    password: Joi.string().required(),
    deviceToken: Joi.string().required(),
    deviceType: Joi.string().required()
});

module.exports.updateProfile = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    verify: Joi.number().optional(),
    name: Joi.string().optional(),
    dateOfBirth: Joi.string().optional(),
    image: Joi.string().optional(),
    address: Joi.object().optional()
});

module.exports.updateAddress = Joi.object({
    id: Joi.string().required(),
    isDelete: Joi.boolean().required(),
    longitude: Joi.string().optional(),
    latitude: Joi.string().optional(),
    houseNo: Joi.string().optional(),
    completeAddress: Joi.string().optional(),
    details: Joi.string().optional(),
    addressType: Joi.number().optional()
});

module.exports.resetPassword = Joi.object({
    newPassword: Joi.string().required()
});

module.exports.changePassword = Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().required()
});

module.exports.sendOTP = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    isForget: Joi.boolean().optional()
});

module.exports.verifyOTP = Joi.object({
    otp: Joi.number().required(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional()
});