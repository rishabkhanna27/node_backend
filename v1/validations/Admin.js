const Joi = require("joi").defaults((schema) => {
    switch (schema.type) {
        case "string":
            return schema.replace(/\s+/, " ");
        default:
            return schema;
    }
});

Joi.objectId = () => Joi.string().pattern(/^[0-9a-f]{24}$/, "valid ObjectId");

module.exports.identify = Joi.object({
    id: Joi.objectId().required()
});

module.exports.register = Joi.object({
    email: Joi.string().email().required().error(new Error("Please Enter a valid email")),
    password: Joi.string().required(),
    name: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    phone: Joi.string().optional(),
    address: Joi.string().optional(),
    image: Joi.string().optional()
});
module.exports.login = Joi.object({
    email: Joi.string().email().required().error(new Error("Please Enter a valid email")),
    password: Joi.string().required()
});
module.exports.updateProfile = Joi.object({
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    name: Joi.string().optional(),
    image: Joi.string().optional(),
    address: Joi.string().optional(),
    isBlocked: Joi.boolean().optional(),
    isDeleted: Joi.boolean().optional()
});
module.exports.changePassword = Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().required()
});
module.exports.resetPassword = Joi.object({
    id: Joi.string().required(),
    newPassword: Joi.string().required()
});
module.exports.forgotPassword = Joi.object({
    email: Joi.string().email().required()
});
module.exports.addUser = Joi.object({
    name: Joi.string().required(),
    password: Joi.string().required(),
    image: Joi.string().required(),
    address: Joi.array().required(),
    dateOfBirth: Joi.string().required(),
    phone: Joi.string().required(),
    countryCode: Joi.string().required(),
    email: Joi.string().email().required()
});
module.exports.addVendor = Joi.object({
    email: Joi.string().email().required(),
    phone: Joi.string().required(),
    countryCode: Joi.string().required(),
    address: Joi.string().required(),
    document: Joi.string().required(),
    name: Joi.string().required(),
    userName: Joi.string().required(),
    image: Joi.string().required(),
    password: Joi.string().optional()
});
module.exports.updateVendor = Joi.object({
    email: Joi.string().email().optional(),
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    address: Joi.string().optional(),
    document: Joi.string().optional(),
    name: Joi.string().optional(),
    userName: Joi.string().optional(),
    image: Joi.string().optional(),
    password: Joi.string().optional(),
    isBlocked: Joi.boolean().optional()
});


