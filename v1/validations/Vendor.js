const Joi = require("joi").defaults((schema) => {
    switch (schema.type) {
        case "string":
            return schema.replace(/\s+/, " ");
        default:
            return schema;
    }
});

Joi.objectId = () => Joi.string().pattern(/^[0-9a-f]{24}$/, "valid ObjectId");

module.exports.identify = Joi.object({
    id: Joi.objectId().required()
});

module.exports.signup = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().required().error(new Error("Please Enter a valid email")),
    name: Joi.string().required(),
    password: Joi.string().optional(),
    confirmPassword: Joi.ref("password"),
    deviceToken: Joi.string().optional(),
    address: Joi.string().optional(),
    document: Joi.string().optional(),
    deviceType: Joi.string().optional()
});

module.exports.socialLogin = Joi.object({
    appleId: Joi.string().optional().allow(""),
    googleId: Joi.string().optional().allow(""),
    facebookId: Joi.string().optional().allow(""),
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    name: Joi.string().optional(),
    deviceToken: Joi.string().optional(),
    deviceType: Joi.string().optional()
});
module.exports.register = Joi.object({
    email: Joi.string().email().required().error(new Error("Please Enter a valid email")),
    password: Joi.string().required(),
    name: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    phone: Joi.string().optional(),
    image: Joi.string().optional()
});
module.exports.login = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    password: Joi.string().required(),
    deviceToken: Joi.string().required(),
    deviceType: Joi.string().required()
});

module.exports.updateProfile = Joi.object({
    name: Joi.string().optional(),
    brandName: Joi.string().optional(),
    bio: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    phone: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    image: Joi.string().optional(),
    startTime: Joi.string().optional(),
    endTime: Joi.string().optional(),
    country: Joi.string().allow("").optional(),
    city: Joi.string().allow("").optional(),
    state: Joi.string().allow("").optional(),
    completeAddress: Joi.string().allow("").optional(),
    address: Joi.string().optional(),
    document: Joi.string().optional(),
    pincode: Joi.string().allow("").optional(),
    longitude: Joi.string().optional(),
    latitude: Joi.string().optional(),
    isNotification: Joi.boolean().optional(),
    isActive: Joi.boolean().optional(),
    geoFence: Joi.string().optional(),
    bankDetails: Joi.object().optional()
});

module.exports.resetPassword = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    otp: Joi.number().required(),
    newPassword: Joi.string().required()
}).xor("email","phone");

module.exports.changePassword = Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().required()
});

module.exports.sendOTP = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    isForget: Joi.boolean().optional()
});

module.exports.verifyOTP = Joi.object({
    otp: Joi.number().required(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional()
});