const Joi = require("joi").defaults((schema) => {
    switch (schema.type) {
        case "string":
            return schema.replace(/\s+/, " ");
        default:
            return schema;
    }
});

Joi.objectId = () => Joi.string().pattern(/^[0-9a-f]{24}$/, "valid ObjectId");

module.exports.headersAllowed = Joi.object({
    
    "host": Joi.string().optional().allow(null, ""),
    "connection": Joi.string().optional().allow(null, ""),
    "sec-ch-ua": Joi.string().optional().allow(null, ""),
    "accept": Joi.string().optional().allow(null, ""),
    "sec-ch-ua-platform": Joi.string().optional().allow(null, ""),
    "sec-ch-ua-mobile": Joi.string().optional().allow(null, ""),
    "authorization": Joi.string().optional().allow(null, ""),
    "user-agent": Joi.string().optional().allow(null, ""),
    "content-type": Joi.string().optional().allow(null, ""),
    "content-length": Joi.string().optional().allow(null, ""),
    "origin": Joi.string().optional().allow(null, ""),
    "sec-fetch-site": Joi.string().optional().allow(null, ""),
    "sec-fetch-mode": Joi.string().optional().allow(null, ""),
    "sec-fetch-dest": Joi.string().optional().allow(null, ""),
    "referer": Joi.string().optional().allow(null, ""),
    "accept-language": Joi.string().optional().allow(null, ""),
    "accept-encoding": Joi.string().optional().allow(null, ""),
    "accept-charset": Joi.string().optional().allow(null, ""),
    "if-none-match": Joi.string().optional().allow(null, ""),
    "postman-token": Joi.string().optional().allow(null, ""),

    "deviceId": Joi.string().required().allow(null, ""),
    "hash": Joi.string().required().allow(null, ""),
    "sek": Joi.string().required().allow(null, "")
});