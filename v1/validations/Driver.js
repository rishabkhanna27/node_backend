const Joi = require("joi").defaults((schema) => {
    switch (schema.type) {
        case "string":
            return schema.replace(/\s+/, " ");
        default:
            return schema;
    }
});

Joi.objectId = () => Joi.string().pattern(/^[0-9a-f]{24}$/, "valid ObjectId");

module.exports.identify = Joi.object({
    id: Joi.objectId().required()
});

module.exports.signup = Joi.object({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    countryCode: Joi.string().required(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    phone: Joi.string().required(),
    age: Joi.number().optional(),
    streetName: Joi.string().allow("").optional(),
    city: Joi.string().allow("").optional(),
    state: Joi.string().allow("").optional(),
    completeAddress: Joi.string().allow("").optional(),
    image: Joi.string().optional(),
    pincode: Joi.string().allow("").optional(),
    verify: Joi.number().optional(),
    password: Joi.string().required(),
    deviceToken: Joi.string().optional(),
    deviceType: Joi.string().optional(),
    longitude: Joi.string().optional(),
    latitude: Joi.string().optional(),
    confirmPassword: Joi.ref("password")
});

module.exports.socialLogin = Joi.object({
    appleId: Joi.string().optional().allow(""),
    googleId: Joi.string().optional().allow(""),
    facebookId: Joi.string().optional().allow(""),
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    verify: Joi.number().optional(),
    firstName: Joi.string().optional(),
    lastName: Joi.string().optional(),
    password: Joi.string().optional(),
    deviceToken: Joi.string().required(),
    deviceType: Joi.string().required()
});

module.exports.login = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    password: Joi.string().required(),
    deviceToken: Joi.string().required(),
    deviceType: Joi.string().required()
});

module.exports.updateProfile = Joi.object({
    firstName: Joi.string().optional(),
    lastName: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    phone: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    verify: Joi.number().optional(),
    age: Joi.number().optional(),
    streetName: Joi.string().allow("").optional(),
    city: Joi.string().allow("").optional(),
    state: Joi.string().allow("").optional(),
    completeAddress: Joi.string().allow("").optional(),
    longitude: Joi.string().optional(),
    latitude: Joi.string().optional(),
    image: Joi.string().optional(),
    pincode: Joi.string().allow("").optional(),
    startTime: Joi.string().optional(),
    endTime: Joi.string().optional(),
    breakSlots: Joi.array().optional(),
    bankName: Joi.string().optional(),
    accountNo: Joi.string().optional(),
    swiftCode: Joi.string().optional(),
    licences: Joi.string().optional(),
    documents: Joi.array().optional(),
    categoryId: Joi.array().optional(),
    productId: Joi.array().optional(),
    addOnId: Joi.array().optional(),
    isNotification: Joi.boolean().optional()
});

module.exports.updateAddress = Joi.object({
    id: Joi.string().required(),
    isDelete: Joi.boolean().required(),
    longitude: Joi.string().optional(),
    latitude: Joi.string().optional(),
    houseNo: Joi.string().optional(),
    completeAddress: Joi.string().optional(),
    details: Joi.string().optional(),
    isPet: Joi.boolean().optional(),
    petDetails: Joi.string().optional(),
    parkingDetails: Joi.string().optional(),
    addressType: Joi.string().optional()
});

module.exports.resetPassword = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    otp: Joi.number().required(),
    newPassword: Joi.string().required()
}).xor("email","phone");

module.exports.changePassword = Joi.object({
    oldPassword: Joi.string().required(),
    newPassword: Joi.string().required()
});

module.exports.sendOTP = Joi.object({
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    isForget: Joi.boolean().optional()
});

module.exports.verifyOTP = Joi.object({
    otp: Joi.number().required(),
    email: Joi.string().email().optional().error(new Error("Please Enter a valid email")),
    phone: Joi.string().optional(),
    countryCode: Joi.string().optional()
});


module.exports.uploadDocuments = Joi.object({
    documentId: Joi.string().required(),
    image: Joi.string().required(),
    backImage: Joi.string().optional(),
    certificateNumber: Joi.string().optional(),
    issueDate: Joi.string().optional(),
    expiryDate: Joi.string().optional()
});