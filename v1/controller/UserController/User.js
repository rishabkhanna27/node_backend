/* eslint-disable no-inner-declarations */
const Model = require("../../../models/index");
const Validation = require("../../validations");
const Auth = require("../../../common/authenticate");
const mongoose = require("mongoose");
const constants = require("../../../common/constants");
const services = require("../../../services/index");
const ObjectId = mongoose.Types.ObjectId;
const functions = require("../../../common/functions");
// const {
//     PdfReader
// } = require("pdfreader");

//Social login using google, facebook and apple.
module.exports.socialLogin = async (req, res, next) => {
    try {
        // await Validation.User.socialLogin.validateAsync(req.body);
        console.log(req.body, "socialLogin...........................");
        const socials = [];
        if (req.body.appleId) {
            socials.push({
                appleId: req.body.appleId
            });
        }
        if (req.body.googleId) {
            socials.push({
                googleId: req.body.googleId
            });
        }
        if (req.body.facebookId) {
            socials.push({
                facebookId: req.body.facebookId
            });
        }
        if (!socials.length) throw new Error(constants.MESSAGES.USER_DATA_MISSING);
        //check if user has already created an account with the social Id's.
        let user = await Model.User.findOne({
            $or: socials,
            isDeleted: false
        });
        let successMessage = 1;
        if (req.body.phoneNo) {
            user = await Model.User.findOne({
                dialCode: req.body.dialCode,
                phoneNo: req.body.phoneNo,
                isDeleted: false
            });
        }
        if (req.body.email) {
            user = await Model.User.findOne({
                email: (req.body.email).toLowerCase(),
                isDeleted: false
            });
        }
        //If no account is created, Craete a new account.
        if (user == null || user == undefined) {
            req.body.isSocialLogin = true;
            user = await Model.User.create(req.body);
            successMessage = 2;
        }
        //Check for the dummy email used by apple if user has hidden his/her email.
        if (user.email) {
            if (req.body.email && !req.body.email.includes("privaterelay.appleid.com")) {
                if (req.body.email) {
                    user.email = req.body.email;
                }
            }
            user.isEmailVerified = true;
        }
        if (user.phone) {
            user.isPhoneVerified = true;
        }

        user.deviceToken = req.body.deviceToken;
        user.deviceType = req.body.deviceType;
        user.loginCount += 1;
        //Create a JTI for secure login using JWT.
        user.jti = functions.generateRandomCustom(25);
        let referedBy = req.body.referedBy || null;
        //Check if user has been referred by another user.
        if (referedBy != null && referedBy != "") {
            let checkRefer = await Model.User.findOne({
                referalCode: referedBy,
                isDeleted: false
            });
            if (checkRefer == null) {
                throw new Error(constants.MESSAGES.ENTER_VALID_CODE);
            }
        }
        user.referalCode = functions.generateRandomCustom(8).toUpperCase();
        user.referedBy = referedBy;
        await user.save();
        user = JSON.parse(JSON.stringify(user));
        //Issue an unique access token to ensure single login.
        user.accessToken = await Auth.getToken({
            _id: user._id,
            role: "user",
            jti: user.jti,
            secretId: req.headers.deviceId
        });
        return res.success(successMessage == 1 ? constants.MESSAGES.LOGIN_SUCCESS : constants.MESSAGES.ACCOUNT_CREATED_SUCCESSFULLY, user);
    } catch (error) {
        next(error);
    }
};
//Signup the user using phone/Email.
module.exports.signup = async (req, res, next) => {
    try {
        // await Validation.User.signup.validateAsync(req.body);
        console.log("req.body.........signup.....", req.body);
        //Check if the phone/email is already used in an account.
        if (req.body.phoneNo) {
            let checkPhone = await Model.User.findOne({
                dialCode: req.body.dialCode,
                phoneNo: req.body.phoneNo,
                isPhoneVerified: true,
                isDeleted: false
            });
            if (checkPhone) {
                throw new Error(constants.MESSAGES.PHONE_ALREADY_IN_USE);
            } else {
                await Model.User.deleteMany({
                    dialCode: req.body.dialCode,
                    phoneNo: req.body.phoneNo,
                    isPhoneVerified: false,
                    isDeleted: false
                });
            }
        }
        if (req.body.email) {
            let checkEmail = await Model.User.findOne({
                email: (req.body.email).toLowerCase(),
                isEmailVerified: true,
                isDeleted: false
            });
            if (checkEmail) {
                throw new Error(constants.MESSAGES.EMAIL_ALREADY_IN_USE);
            } else {
                await Model.User.deleteMany({
                    email: (req.body.email).toLowerCase(),
                    isEmailVerified: false,
                    isDeleted: false
                });
            }
        }
        let dataToSave = req.body;
        //Create a user and check using which verification method user wants to very his/her account.
        let user = await Model.User.create(dataToSave);
        //Send verification code using Sms service or Email service.
        if (req.body.verify == constants.VERIFY.EMAIL) {
            let payload = {
                email: (req.body.email).toLowerCase(),
                name: req.body.firstName ? req.body.firstName : req.body.email
            };
            services.EmalService.sendEmailVerification(payload);
        } else if (req.body.verify == constants.VERIFY.PHONE) {
            let payload = {
                dialCode: user.dialCode,
                phoneNo: user.phoneNo
            };
            services.EmalService.sendPhoneVerification(payload);
        }
        let referedBy = req.body.referedBy || null;
        //Check if the user has been referred by some another user.
        if (referedBy != null && referedBy != "") {
            let checkRefer = await Model.User.findOne({
                referalCode: referedBy,
                isDeleted: false
            });
            if (checkRefer == null) {
                throw new Error(constants.MESSAGES.ENTER_VALID_CODE);
            }
        }
        user.referalCode = functions.generateRandomCustom(8).toUpperCase();
        user.referedBy = referedBy;
        //Decode the password using Bcrypt to ensure secure login.
        if (req.body.password) {
            user.setPassword(req.body.password);
        }
        await user.save();
        return res.success(constants.MESSAGES.ACCOUNT_CREATED_SUCCESSFULLY, user);
    } catch (error) {
        next(error);
    }
};
//Login the user using phone/Email.
module.exports.login = async (req, res, next) => {
    try {
        // await Validation.User.login.validateAsync(req.body);
        console.log("req.body.......login.......", req.body);
        //Check if the user exists in the database.
        let user = null;
        if (req.body.email) {
            user = await Model.User.findOne({
                email: (req.body.email).toLowerCase(),
                isDeleted: false
            });
        } else if (req.body.phoneNo) {
            user = await Model.User.findOne({
                dialCode: req.body.dialCode,
                phoneNo: req.body.phoneNo,
                isDeleted: false
            });
        }
        if (user == null) throw new Error(constants.MESSAGES.INVALID_CREDENTIALS);
        if (user.isBlocked) throw new Error(constants.MESSAGES.ACCOUNT_BLOCKED);
        //If user account is not verified send a verification code to the user.
        if (user.isEmailVerified == false) {
            let payload = {
                email: (req.body.email).toLowerCase(),
                name: user.firstName ? user.firstName : user.email
            };
            services.EmalService.sendEmailVerification(payload);
            return res.success("Verification code sent on email.", user);
        }
        await user.authenticate(req.body.password);
        user.loginCount += 1;
        user.deviceType = req.body.deviceType;
        user.deviceToken = req.body.deviceToken;
        //Re create a new JTI for JWT to ensure single login.
        user.jti = functions.generateRandomCustom(25);
        await user.save();
        user = JSON.parse(JSON.stringify(user));
        user.accessToken = await Auth.getToken({
            _id: user._id,
            role: "user",
            jti: user.jti,
            secretId: req.headers.deviceId
        });
        return res.success(constants.MESSAGES.LOGIN_SUCCESS, user);
    } catch (error) {
        next(error);
    }
};
//Logout the current user and change the JTI, Also remove the current device type and device token. 
module.exports.logout = async (req, res, next) => {
    try {
        let jti = functions.generateRandomCustom(25);
        await Model.User.updateOne({
            _id: req.user._id,
            isDeleted: false
        }, {
            deviceType: "",
            deviceToken: "",
            jti: jti
        });
        return res.success(constants.MESSAGES.LOGOUT_SUCCESS);
    } catch (error) {
        next(error);
    }
};
//Get the complete profile of the current user.
module.exports.getProfile = async (req, res, next) => {
    try {
        let doc = await Model.User.findOne({
            _id: req.user._id,
            isDeleted: false
        }, {
            password: 0
        });
        return res.success(constants.MESSAGES.DATA_FETCHED, doc);
    } catch (error) {
        next(error);
    }
};
//Delete the profile of the current user.
module.exports.deleteProfile = async (req, res, next) => {
    try {
        await Model.User.findOneAndUpdate({
            _id: req.user._id
        }, {
            $set: {
                isDeleted: true
            }
        }, {
            new: true
        });
        return res.success(constants.MESSAGES.ACCOUNT_DELETED);
    } catch (error) {
        next(error);
    }
};
//Update the user details.
module.exports.updateProfile = async (req, res, next) => {
    try {
        // await Validation.User.updateProfile.validateAsync(req.body);
        console.log(req.body, "updateProfile.............");
        const nin = {
            $nin: [req.user._id]
        };
        if (req.body.phoneNo) {
            let checkPhone = await Model.User.findOne({
                _id: nin,
                dialCode: req.body.dialCode,
                phoneNo: req.body.phoneNo,
                isDeleted: false
            });
            if (checkPhone) {
                throw new Error(constants.MESSAGES.PHONE_ALREADY_IN_USE);
            } else {
                checkPhone = await Model.User.findOne({
                    _id: req.user._id,
                    dialCode: req.body.dialCode,
                    phoneNo: req.body.phoneNo,
                    isDeleted: false,
                    isPhoneVerified: true
                });
                if (checkPhone == null) {
                    req.body.isPhoneVerified = false;
                    let dataToSend = {
                        phoneNo: req.body.phoneNo,
                        dialCode: req.body.dialCode
                    };
                    services.SmsService.sendPhoneVerification(dataToSend);
                }
            }
        }
        if (req.body.email) {
            let checkEmail = await Model.User.findOne({
                _id: nin,
                email: req.body.email.toLowerCase(),
                isEmailVerified: true,
                isDeleted: false
            });
            if (checkEmail) {
                throw new Error(constants.MESSAGES.EMAIL_ALREADY_IN_USE);
            } else {
                checkEmail = await Model.User.findOne({
                    _id: req.user._id,
                    email: (req.body.email).toLowerCase(),
                    isDeleted: false,
                    isEmailVerified: true
                });
                if (checkEmail == null) {
                    req.body.isEmailVerified = false;
                    let dataToSend = {
                        email: (req.body.email).toLowerCase(),
                        name: checkEmail ? checkEmail.firstName : req.body.email

                    };
                    services.EmalService.sendEmailVerification(dataToSend);
                }
            }
        }
        if (req.body.image == "") {
            delete req.body.image;
        }
        //If user tries to remove the password them delete the password key.
        if (req.body.password == "" || req.body.password == null) {
            delete req.body.password;
        }
        const userData = await Model.User.findOne({
            _id: req.user._id,
            isDeleted: false
        });
        if (userData == null) throw new Error(constants.MESSAGES.USER_DATA_MISSING);
        if (userData.isEmailVerified == true && req.body.email == "") {
            delete req.body.email;
        }
        if (userData.isPhoneVerified == true && req.body.phoneNo == "") {
            delete req.body.phoneNo;
            delete req.body.dialCode;
        }
        if (req.body.dialCode && !req.body.dialCode.includes("+")) {
            req.body.dialCode = "+" + req.body.dialCode;
        }
        let updated = await Model.User.findOneAndUpdate({
            _id: req.user._id,
            isDeleted: false
        }, {
            $set: req.body
        }, {
            new: true
        });
        return res.success(constants.MESSAGES.PROFILE_UPDATED_SUCCESSFULLY, updated);

    } catch (error) {
        next(error);
    }
};
//Reset password in case of forgot.
module.exports.resetPassword = async (req, res, next) => {
    try {
        // await Validation.User.resetPassword.validateAsync(req.body);
        const doc = await Model.User.findOne({
            _id: req.user._id
        });
        if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);
        await doc.setPassword(req.body.newPassword);
        await doc.save();
        return res.success(constants.MESSAGES.PASSWORD_RESET);
    } catch (error) {
        next(error);
    }
};
//Change the old password with the new one.
module.exports.changePassword = async (req, res, next) => {
    try {
        await Validation.User.changePassword.validateAsync(req.body);
        if (req.body.oldPassword == req.body.newPassword)
            throw new Error(constants.MESSAGES.PASSWORDS_SHOULD_BE_DIFFERENT);

        const doc = await Model.User.findOne({
            _id: req.user._id
        });
        if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);

        await doc.authenticate(req.body.oldPassword);
        await doc.setPassword(req.body.newPassword);
        await doc.save();

        return res.success(constants.MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY);
    } catch (error) {
        next(error);
    }
};

//Send/Resend Otp to the user for verifcation
module.exports.sendOtp = async (req, res, next) => {
    try {
        console.log(req.body, "sendOtp................");
        await Validation.User.sendOTP.validateAsync(req.body);
        //Send Otp in case of forgot password.
        if (req.body.isForget == true) {
            if (req.body.phoneNo) {
                let dataToSend = {
                    phoneNo: req.body.phoneNo,
                    dialCode: req.body.dialCode,
                    isDeleted: false
                };
                let check = await Model.User.findOne(dataToSend);
                if (check == null) throw new Error("Account not found.");
                services.SmsService.sendPhoneVerification(dataToSend);
            }
            if (req.body.email) {
                let check = await Model.User.findOne({
                    email: req.body.email,
                    isDeleted: false
                });
                if (check == null) throw new Error("Account not found.");
                let dataToSend = {
                    email: (req.body.email).toLowerCase(),
                    name: check.firstName ? check.firstName : check.email,
                    title: "Verify OTP for Forgot password"
                };
                services.EmalService.sendEmailVerification(dataToSend);
            }
        }
        //Send Otp in case of verification.
        else {
            if (req.body.phoneNo) {
                let dataToSend = {
                    phoneNo: req.body.phoneNo,
                    dialCode: req.body.dialCode
                };
                services.SmsService.sendPhoneVerification(dataToSend);
            }
            if (req.body.email) {
                let check = await Model.User.findOne({
                    email: req.body.email
                });
                let dataToSend = {
                    email: (req.body.email).toLowerCase(),
                    name: check.firstName ? check.firstName : check.email
                };
                services.EmalService.sendEmailVerification(dataToSend);
            }
        }
        return res.success(constants.MESSAGES.OTP_SENT);
    } catch (error) {
        next(error);
    }
};
//Verify the user otp with email/phone.
module.exports.verifyOtp = async (req, res, next) => {
    try {
        console.log(req.body, "verifyOtp.................");
        await Validation.User.verifyOTP.validateAsync(req.body);
        let data = null;
        let qry = {
            otp: req.body.otp
        };
        if (req.body.email) {
            qry.email = req.body.email;
        } else {
            qry.phoneNo = req.body.phoneNo;
        }
        if (req.body.dialCode) {
            qry.dialCode = req.body.dialCode;
        }
        //Check if user has sent any otp for verification.
        let otp = await Model.Otp.findOne(qry);
        if (!otp) {
            throw new Error(constants.MESSAGES.INVALID_OTP);
        }
        if (otp)
            await Model.Otp.findByIdAndRemove(otp._id);
        if (req.body.email) {
            data = await Model.User.findOneAndUpdate({
                email: (req.body.email).toLowerCase(),
                isDeleted: false
            }, {
                $set: {
                    isEmailVerified: true
                }
            }, {
                new: true
            });
        } else {
            data = await Model.User.findOneAndUpdate({
                phoneNo: req.body.phoneNo,
                dialCode: req.body.dialCode,
                isDeleted: false
            }, {
                $set: {
                    isPhoneVerified: true
                }
            }, {
                new: true
            });
        }
        if (data == null) {
            throw new Error("Account not found.");
        }
        data.jti = functions.generateRandomCustom(25);
        await data.save();
        data = JSON.parse(JSON.stringify(data));
        data.accessToken = await Auth.getToken({
            _id: data._id,
            role: "user",
            jti: data.jti,
            secretId: req.headers.deviceId
        });
        return res.success(req.body.email ? constants.MESSAGES.ACCOUNT_VERIFIED :
            constants.MESSAGES.ACCOUNT_PHONE, data);
    } catch (error) {
        next(error);
    }
};

//Address
module.exports.addUserAddress = async (req, res, next) => {
    try {
        req.body.userId = req.user._id;
        if (req.body.longitude && req.body.latitude) {
            let checkAddress = await Model.City.aggregate([{
                $match: {
                    isDeleted: false,
                    isBlocked: false,
                    geoFence: {
                        $geoIntersects: {
                            $geometry: {
                                type: "Point",
                                coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.latitude)]
                            }
                        }
                    }
                }
            }]);
            if (checkAddress && checkAddress.length == 0) {
                throw new Error("We do not server at this location please try again later!");
            }
            req.body.location = {
                type: "Point",
                coordinates: [req.body.longitude, req.body.latitude]
            };
            req.body.cityId = checkAddress[0]._id;
        }
        let address = await Model.UserAddress.create(req.body);
        return res.success(constants.MESSAGES.SUCCESS, address);
    } catch (error) {
        next(error);
    }

};
module.exports.getAddress = async (req, res, next) => {
    try {
        let page = null;
        let limit = null;
        page = req.body.page ? Number(req.body.page) : 1;
        limit = req.body.limit ? Number(req.body.limit) : 10;
        let skip = Number((page - 1) * limit);
        let id = req.params.id;
        if (id == null) {
            let address = await Model.UserAddress.find({
                userId: req.user._id
            }).sort({
                createdAt: -1
            }).skip(skip).limit(limit).populate("userId");
            let totaluserAddress = await Model.UserAddress.countDocuments({
                userId: req.user._id
            });

            return res.success(constants.MESSAGES.DATA_FETCHED, {
                address,
                totaluserAddress
            });
        } else {
            let address = await Model.UserAddress.findOne({
                _id: ObjectId(req.params.id),
                isDeleted: false
            }).populate("userId");
            if (address == null) throw new Error(constants.MESSAGES.USER_DATA_MISSING);
            return res.success(constants.MESSAGES.DATA_FETCHED, address);
        }

    } catch (error) {
        next(error);
    }
};
module.exports.updatedAddress = async (req, res, next) => {
    try {
        const updatedAddress = await Model.UserAddress.findOneAndUpdate({
            _id: ObjectId(req.params.id)
        }, {
            $set: req.body
        }, {
            new: true
        });
        return res.success(constants.MESSAGES.SUCCESS, updatedAddress);

    } catch (error) {
        next(error);
    }
};
module.exports.deleteAddress = async (req, res, next) => {
    try {
        await Model.UserAddress.deleteOne({
            _id: ObjectId(req.params.id)
        });
        return res.success(constants.MESSAGES.SUCCESS);
    } catch (error) {
        next(error);
    }
};

//Cms
module.exports.getCms = async (req, res, next) => {
    try {
        const cmsData = await Model.Cms.findOne({});
        return res.success(constants.MESSAGES.DATA_FETCHED, cmsData);
    } catch (error) {
        next(error);
    }
};

//PDF
module.exports.test = async (req, res, next) => {
    try {
        return res.success("Success");
    } catch (error) {
        next(error);
    }
};