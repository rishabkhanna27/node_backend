const Model = require("../../../models/index");
const Validation = require("../../validations");
const Auth = require("../../../common/authenticate");
const mongoose = require("mongoose");
const constants = require("../../../common/constants");
const smsService = require("../../../services/SmsService");
const ObjectId = mongoose.Types.ObjectId;
const functions = require("../../../common/functions");

//Onboarding

module.exports.signupOtp = async (req, res, next) => {
  try {
    await Validation.User.validateSendOtp.validateAsync(req.body);
    let userPhoneExist = await Model.Driver.findOne({
      phone: req.body.phone || req.body.key,
      countryCode: req.body.countryCode,
      isDeleted: false
    });
    if (userPhoneExist) {
      throw new Error("Phone number already exists");
    }
    if (req.body.email) {
      let userEmailExist = await Model.Driver.findOne({
        isDeleted: false,
        email: req.body.email
      });
      if (userEmailExist) {
        throw new Error("Email already exists");
      }
    }
    let otp = await smsService.sendPhoneVerification({
      countryCode: req.body.countryCode,
      phone: req.body.phone || req.body.key
    });
    if (!otp) {
      throw new Error("Otp not sent!");
    }
    return res.success("Success", {
      verificationType: 0,
      otp: null
    });
  } catch (error) {
    next(error);
  }
};
module.exports.createUser = async (req, res, next) => {
  try {
    if ((!req.body.email && !req.body.phone) || req.body.key) {
      throw new Error("Required fields are missing!");
    }
    req.body.myReferralCode = functions.generateRandomStringAndNumbers(10);
    if (req.body.email) {
      let check = await Model.Driver.findOne({
        email: req.body.email.toLowerCase(),
        isDeleted: false
      });
      if (check) {
        throw new Error("Email is already in use!");
      }
    }
    if (req.body.phone || req.body.key) {
      let checkPhone = await Model.Driver.findOne({
        phone: req.body.phone || req.body.key,
        countryCode: req.body.countryCode,
        isDeleted: false
      });
      if (checkPhone) {
        throw new Error("Phone is already in use!");
      }
    }
    let checkOtp = await Model.Otp.findOne({
      phone: req.body.phone || req.body.key,
      countryCode: req.body.countryCode,
      otp: req.body.code
    });
    if (!checkOtp) {
      throw new Error("Invalid Otp!");
    }
    await Model.Otp.deleteMany({
      phone: req.body.phone || req.body.key,
      countryCode: req.body.countryCode
    });
    req.body.verificationType = 0;
    req.body.isPhoneVerify = true;
    let user = await Model.Driver.create(req.body);
    user.setPassword(req.body.password);
    user.jti = functions.generateRandomStringAndNumbers(25);
    user.loginCount += 1;
    await user.save();

    let token = await Auth.getToken({
      _id: user._id,
      role: "driver",
      jti: user.jti,
      secretId: req.headers.deviceId
    });
    let tokenType = "Bearer";
    let tokenExpire = 1676973178;
    let tokenzRefreshToken = token;
    return res.success("Success", {
      email: user.email,
      phone: user.phone,
      countryCode: user.countryCode,
      countryISOCode: user.countryISOCode,
      _id: user._id,
      token: token,
      type: tokenType,
      expire: tokenExpire,
      refreshToken: tokenzRefreshToken
    });
  } catch (error) {
    next(error);
  }
};
module.exports.updateProfile = async (req, res, next) => {
  try {
    console.log(req.body, "111111");
    if (req.body.email) {
      let check = await Model.Driver.findOne({
        _id: {
          $ne: req.driver._id
        },
        email: req.body.email.toLowerCase(),
        isDeleted: false
      });
      if (check) {
        throw new Error("Email is already in use!");
      }
    }
    if (req.body.phone || req.body.key) {
      let checkPhone = await Model.Driver.findOne({
        _id: {
          $ne: req.driver._id
        },
        phone: req.body.phone || req.body.key,
        countryCode: req.body.countryCode,
        isDeleted: false
      });
      if (checkPhone) {
        throw new Error("Phone is already in use!");
      }
    }
    if (req.body.vehicleDetails) {
      req.body.isVehicleUpdated = true;
    }
    if (req.body.bankDetails) {
      req.body.isBankUpdated = true;
    }
    if (req.body.longitude && req.body.latitude) {
      let checkAddress = await Model.City.aggregate([{
        $match: {
          isDeleted: false,
          isBlocked: false,
          geoFence: {
            $geoIntersects: {
              $geometry: {
                type: "Point",
                coordinates: [parseFloat(req.body.longitude), parseFloat(req.body.latitude)]
              }
            }
          }
        }
      }]);
      if (checkAddress && checkAddress.length == 0) {
        throw new Error("We do not server at this location please try again later!");
      }
      req.body.location = {
        type: "Point",
        coordinates: [req.body.longitude, req.body.latitude]
      };
      req.body.cityId = checkAddress[0]._id;
    }
    req.body.isProfileComplete = true;
    let updatedUser = await Model.Driver.findOneAndUpdate({
        _id: req.driver._id
      }, req.body, {
        new: true
      })
      .lean();
    updatedUser.token = req.headers.authorization;

    return res.success("Success", updatedUser);
  } catch (error) {
    next(error);
  }
};
module.exports.logout = async (req, res, next) => {
  try {
    await Model.Driver.findByIdAndUpdate(req.driver._id, {
      $set: {
        deviceType: "",
        deviceToken: "",
        jti: ""
      }
    }, {
      new: true
    });
    return res.success("Success", "Logged out sucessfully");
  } catch (error) {
    next(error);
  }
};
module.exports.forgotPassword = async (req, res, next) => {
  try {
    if (req.body.phone || req.body.key) {
      let user = await Model.Driver.findOne({
        phone: req.body.phone || req.body.key,
        countryCode: req.body.countryCode,
        isDeleted: false
      });
      if (!user) {
        throw new Error("Phone number does not exist");
      }
      let otp = await smsService.sendPhoneVerification({
        countryCode: req.body.countryCode,
        phone: req.body.phone || req.body.key
      });
      if (!otp) {
        throw new Error("Otp not sent!");
      }
      return res.success("Success", {
        verificationType: 0,
        otp: null
      });
    } else {
      throw new Error("Invalid type!");
    }
  } catch (error) {
    next(error);
  }

};
module.exports.verifyOtp = async (req, res, next) => {
  try {
    console.log(req.body, "3213213213");
    let qry = {
      otp: req.body.code
    };
    if (req.body.email) {
      qry.email = req.body.email;
    } else {
      qry.phone = req.body.phone || req.body.key;
    }
    if (req.body.countryCode) {
      qry.countryCode = req.body.countryCode;
    }
    //Check if user has sent any otp for verification.
    let otp = await Model.Otp.findOne(qry);
    if (!otp) {
      throw new Error(constants.MESSAGES.INVALID_OTP);
    }
    await Model.Otp.deleteOne({
      _id: otp._id
    });
    let data = null;
    if (req.body.email) {
      data = await Model.Driver.findOneAndUpdate({
        email: (req.body.email).toLowerCase(),
        isDeleted: false
      }, {
        $set: {
          isEmailVerify: true
        }
      }, {
        new: true
      });
    } else {
      data = await Model.Driver.findOneAndUpdate({
        phone: req.body.phone || req.body.key,
        countryCode: req.body.countryCode,
        isDeleted: false
      }, {
        $set: {
          isPhoneVerify: true
        }
      }, {
        new: true
      });
    }
    if (data == null) {
      throw new Error("Account not found.");
    }
    data.loginCount += 1;
    data.jti = functions.generateRandomStringAndNumbers(25);
    await data.save();
    let token = await Auth.getToken({
      _id: data._id,
      role: "driver",
      jti: data.jti,
      secretId: req.headers.deviceId
    });

    return res.success("Success", {
      token: token,
      type: "Bearer"
    });
  } catch (error) {
    next(error);
  }
};
module.exports.resetPassword = async (req, res, next) => {
  try {
    const doc = await Model.Driver.findOne({
      _id: req.driver._id
    });
    if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);
    await doc.setPassword(req.body.password);
    await doc.save();
    return res.success(constants.MESSAGES.PASSWORD_RESET);
  } catch (error) {
    next(error);
  }
};
module.exports.changePassword = async (req, res, next) => {
  try {
    if (req.body.oldPassword == req.body.password)
      throw new Error(constants.MESSAGES.PASSWORDS_SHOULD_BE_DIFFERENT);

    const doc = await Model.Driver.findOne({
      _id: req.driver._id
    });
    if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);

    await doc.authenticate(req.body.oldPassword);
    await doc.setPassword(req.body.password);
    await doc.save();

    return res.success(constants.MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY);
  } catch (error) {
    next(error);
  }
};
module.exports.login = async (req, res, next) => {
  try {
    console.log("req.body.......login.......", req.body);
    //Check if the user exists in the database.
    let user = null;
    if (req.body.email) {
      user = await Model.Driver.findOne({
        email: (req.body.email).toLowerCase(),
        isDeleted: false
      });
    } else if (req.body.phone || req.body.key) {
      user = await Model.Driver.findOne({
        countryCode: req.body.countryCode,
        phone: req.body.phone || req.body.key,
        isDeleted: false
      });
    }
    if (user == null) throw new Error(constants.MESSAGES.INVALID_CREDENTIALS);
    if (user.isBlocked) throw new Error(constants.MESSAGES.ACCOUNT_BLOCKED);
    //If user account is not verified send a verification code to the user.
    if (user.isPhoneVerify == false) {
      let payload = {
        countryCode: user.countryCode,
        phone: user.phone
      };
      smsService.sendPhoneVerification(payload);
      return res.success("Verification code sent on phone.", user);
    }
    await user.authenticate(req.body.password);
    user.loginCount += 1;
    user.deviceType = req.body.deviceType;
    user.deviceToken = req.body.deviceToken;
    user.jti = functions.generateRandomStringAndNumbers(25);
    user.loginCount += 1;
    await user.save();
    user = JSON.parse(JSON.stringify(user));
    user.accessToken = await Auth.getToken({
      _id: user._id,
      role: "driver",
      jti: user.jti,
      secretId: req.headers.deviceId
    });
    return res.success(constants.MESSAGES.LOGIN_SUCCESS, user);
  } catch (error) {
    next(error);
  }
};
module.exports.getProfile = async (req, res, next) => {
  try {
    const doc = await Model.Driver.findOne({
      _id: req.driver._id
    });
    if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);
    delete doc.password;
    if (doc.isBlocked) throw new Error(constants.MESSAGES.ACCOUNT_BLOCKED);
    return res.success(constants.MESSAGES.DATA_FETCHED, doc);
  } catch (error) {
    next(error);
  }
};

//Documents
module.exports.requiredDocument = async (req, res, next) => {
  try {
    let data = await Model.RequiredDocuments.find({
      role: "driver"
    });
    return res.success("Success", data);
  } catch (error) {
    next(error);
  }
};
module.exports.getDriverDocuments = async (req, res, next) => {
  try {
    let documents = await Model.DriverDocuments.find({
      driverId: req.driver._id
    });
    return res.success("Success", documents);
  } catch (error) {
    next(error);
  }
};
module.exports.uploadDocuments = async (req, res, next) => {
  try {
    await Validation.Driver.uploadDocuments.validateAsync(req.body);
    let documents = await Model.RequiredDocuments.findOne({
      _id: ObjectId(req.body.documentId)
    });
    if (documents == null) {
      throw new Error("Document is not required");
    }
    req.body.driverId = req.driver._id;
    let documentData = await Model.DriverDocuments(req.body).save();
    await Model.Driver.findOneAndUpdate({
        _id: req.driver._id
      }, {
        isDocumentUploaded: true
      }, {
        new: true
      })
      .lean();
    return res.success("Success", documentData);
  } catch (error) {
    next(error);
  }
};

//Vehicle Type
module.exports.getVehicleType = async (req, res, next) => {
  try {
    let page = null;
    let limit = null;
    page = req.body.page ? Number(req.body.page) : 1;
    limit = req.body.limit ? Number(req.body.limit) : 10;
    let skip = Number((page - 1) * limit);
    let id = req.params.id;
    if (id == null) {
      let qry = {};
      if (req.body.search) {
        const regex = new RegExp(req.body.search, "i");
        qry._search = regex;
      }
      let pipeline = [{
          $match: {
            isDeleted: false
          }
        },
        {
          $addFields: {
            _search: {
              $concat: ["$name"]
            }
          }
        },
        {
          $match: qry
        },
        {
          $sort: {
            createdAt: -1
          }
        },
        {
          $skip: skip
        },
        {
          $limit: limit
        }
      ];
      let countPipeline = [{
          $match: {
            isDeleted: false
          }
        },
        {
          $addFields: {
            _search: {
              $concat: ["$name"]
            }
          }
        },
        {
          $match: qry
        }
      ];
      let vehicleType = await Model.VehicleType.aggregate(pipeline);
      let totalVehicleType = await Model.VehicleType.aggregate(countPipeline);
      totalVehicleType = totalVehicleType.length;

      return res.success(constants.MESSAGES.DATA_FETCHED, {
        vehicleType,
        totalVehicleType
      });
    } else {
      let vehicleType = await Model.VehicleType.findOne({
        _id: ObjectId(req.params.id),
        isDeleted: false
      });
      if (vehicleType == null) throw new Error(constants.MESSAGES.USER_DATA_MISSING);
      return res.success(constants.MESSAGES.DATA_FETCHED, vehicleType);
    }

  } catch (error) {
    next(error);
  }
};