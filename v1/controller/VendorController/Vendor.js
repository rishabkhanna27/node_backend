const Model = require("../../../models");
const Auth = require("../../../common/authenticate");
const mongoose = require("mongoose");
const constants = require("../../../common/constants");
const functions = require("../../../common/functions");
const emailService = require("../../../services/EmalService");
const ObjectId = mongoose.Types.ObjectId;

//Signup the admin using email.
module.exports.register = async (req, res, next) => {
    try {
        if (req.body.email) {
            const checkEmail = await Model.Vendor.findOne({
                email: (req.body.email).toLowerCase(),
                isDeleted: false
            });
            if (checkEmail) throw new Error(constants.MESSAGES.EMAIL_ALREADY_IN_USE);
        }
        if (req.body.userName) {
            const checkUserName = await Model.Vendor.findOne({
                userName: req.body.userName,
                isDeleted: false
            });
            if (checkUserName) throw new Error("Username already exists");
        }
        const create = await Model.Vendor(req.body).save();
        //Convert password to hash using bcrypt.
        await create.setPassword(req.body.password);
        await create.save();
        delete create.password;
        return res.success(constants.MESSAGES.PROfILE_CREATED_SUCCESSFULLY, create);
    } catch (error) {
        next(error);
    }
};
//Login the admin using phone/Email.
module.exports.login = async (req, res, next) => {
    try {
        let doc = await Model.Vendor.findOne({
            email: (req.body.email).toLowerCase(),
            isDeleted: false
        });
        if (!doc) {
            throw new Error(constants.MESSAGES.INVALID_CREDENTIALS);
        }
        await doc.authenticate(req.body.password);
        if (doc.isBlocked) {
            throw new Error(constants.MESSAGES.ACCOUNT_BLOCKED);
        }
        //Create a new JTI for single session timeout
        doc.loginCount += 1;
        doc.jti = functions.generateRandomStringAndNumbers(25);

        await doc.save();
        doc = JSON.parse(JSON.stringify(doc));
        doc.accessToken = await Auth.getToken({
            _id: doc._id,
            jti: doc.jti,
            role: "vendor",
            secretId: req.headers.deviceId
        });
        delete doc.password;
        return res.success(constants.MESSAGES.LOGIN_SUCCESS, doc);
    } catch (error) {
        next(error);
    }
};
//Logout the current admin and change the JTI, Also remove the current device type and device token. 
module.exports.logout = async (req, res, next) => {
    try {
        await Model.Vendor.updateOne({
            _id: req.vendor._id
        }, {
            deviceType: "",
            deviceToken: "",
            jti: ""
        });
        return res.success(constants.MESSAGES.LOGOUT_SUCCESS);
    } catch (error) {
        next(error);
    }
};
//Get the complete profile of the current admin.
module.exports.getProfile = async (req, res, next) => {
    try {
        const doc = await Model.Vendor.findOne({
            _id: req.vendor._id
        }, {
            password: 0
        });
        if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);
        if (doc.isBlocked) throw new Error(constants.MESSAGES.ACCOUNT_BLOCKED);
        return res.success(constants.MESSAGES.DATA_FETCHED, doc);
    } catch (error) {
        next(error);
    }
};
//Update the admin details.
module.exports.updateProfile = async (req, res, next) => {
    try {
        const nin = {
            $nin: [req.vendor._id]
        };
        if (req.body.email) {
            const checkEmail = await Model.Vendor.findOne({
                _id: nin,
                email: (req.body.email).toLowerCase(),
                isDeleted: false
            });
            if (checkEmail) throw new Error(constants.MESSAGES.EMAIL_ALREADY_IN_USE);
        }
        if (req.body.phone) {
            const checkPhone = await Model.Vendor.findOne({
                _id: nin,
                countryCode: req.body.countryCode,
                phone: req.body.phone,
                isDeleted: false
            });
            if (checkPhone) throw new Error(constants.MESSAGES.PHONE_ALREADY_IN_USE);
        }
        if (req.body.userName) {
            const checkUserName = await Model.Vendor.findOne({
                userName: req.body.userName,
                isDeleted: false
            });
            if (checkUserName) throw new Error("Username already exists");
        }
        delete req.body.password;
        const updated = await Model.Vendor.findOneAndUpdate({
            _id: req.vendor._id
        }, {
            $set: req.body
        }, {
            new: true
        });
        return res.success(constants.MESSAGES.PROFILE_UPDATED_SUCCESSFULLY, updated);
    } catch (error) {
        next(error);
    }
};
//Change the old password with the new one.
module.exports.changePassword = async (req, res, next) => {
    try {
        if (req.body.oldPassword == req.body.newPassword)
            throw new Error(constants.MESSAGES.PASSWORDS_SHOULD_BE_DIFFERENT);

        const doc = await Model.Vendor.findOne({
            _id: req.vendor._id
        });
        if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);

        await doc.authenticate(req.body.oldPassword);
        await doc.setPassword(req.body.newPassword);
        await doc.save();

        return res.success(constants.MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY);
    } catch (error) {
        next(error);
    }
};
//Reset password in case of forgot.
module.exports.resetPassword = async (req, res, next) => {
    try {
        const doc = await Model.Vendor.findOne({
            _id: ObjectId(req.body.id),
            isDeleted: false
        });
        if (!doc) throw new Error(constants.MESSAGES.USER_DATA_MISSING);
        await doc.setPassword(req.body.newPassword);
        await doc.save();
        return res.success(constants.MESSAGES.PASSWORD_RESET);
    } catch (error) {
        next(error);
    }
};
//Send link to the admin on email to reset the password.
module.exports.forgotPassword = async (req, res, next) => {
    try {
        const check = await Model.Vendor.findOne({
            email: (req.body.email).toLowerCase(),
            isDeleted: false
        });
        if (check == null) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);
        const link = `https://appgrowthcompany.com/asap/vendor/auth/reset/${check._id}`;
        let payload = {
            link: link,
            email: (req.body.email).toLowerCase()
        };
        await emailService.sendForgotLink(payload);
        return res.success(constants.MESSAGES.LINK_SENT);
    } catch (error) {
        next(error);
    }
};