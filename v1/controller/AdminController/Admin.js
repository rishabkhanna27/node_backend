const Model = require("./../../../models/");
const Auth = require("../../../common/authenticate");
const Validation = require("../../validations");
const mongoose = require("mongoose");
const constants = require("../../../common/constants");
const functions = require("../../../common/functions");
const emailService = require("../../../services/EmalService");
const ObjectId = mongoose.Types.ObjectId;


//Signup the admin using email.
module.exports.register = async (req, res, next) => {
    try {
        await Validation.Admin.register.validateAsync(req.body);
        if (req.body.email) {
            const checkEmail = await Model.Admin.findOne({
                email: (req.body.email).toLowerCase(),
                isDeleted: false
            });
            if (checkEmail) throw new Error(constants.MESSAGES.EMAIL_ALREADY_IN_USE);
        }
        req.body.role = constants.ROLE.ADMIN;
        const create = await Model.Admin(req.body).save();
        //Convert password to hash using bcrypt.
        await create.setPassword(req.body.password);
        await create.save();
        delete create.password;
        return res.success(constants.MESSAGES.PROfILE_CREATED_SUCCESSFULLY, create);
    } catch (error) {
        next(error);
    }
};
//Login the admin using phone/Email.
module.exports.login = async (req, res, next) => {
    try {
        await Validation.Admin.login.validateAsync(req.body);
        let doc = await Model.Admin.findOne({
            email: (req.body.email).toLowerCase(),
            isDeleted: false
        });
        if (!doc) {
            throw new Error(constants.MESSAGES.INVALID_CREDENTIALS);
        }
        await doc.authenticate(req.body.password);
        if (doc.isBlocked) {
            throw new Error(constants.MESSAGES.ACCOUNT_BLOCKED);
        }
        //Create a new JTI for single session timeout
        doc.loginCount += 1;
        doc.jti = functions.generateRandomStringAndNumbers(25);

        await doc.save();
        doc = JSON.parse(JSON.stringify(doc));
        doc.accessToken = await Auth.getToken({
            _id: doc._id,
            jti: doc.jti,
            role: "admin",
            secretId: req.headers.deviceId
        });

        delete doc.password;
        return res.success(constants.MESSAGES.LOGIN_SUCCESS, doc);
    } catch (error) {
        next(error);
    }
};
//Logout the current admin and change the JTI, Also remove the current device type and device token. 
module.exports.logout = async (req, res, next) => {
    try {
        await Model.Admin.updateOne({
            _id: req.admin._id
        }, {
            deviceType: "",
            deviceToken: "",
            jti: ""
        });
        return res.success(constants.MESSAGES.LOGOUT_SUCCESS);
    } catch (error) {
        next(error);
    }
};
//Get the complete profile of the current admin.
module.exports.getProfile = async (req, res, next) => {
    try {
        const doc = await Model.Admin.findOne({
            _id: req.admin._id
        }, {
            password: 0
        });
        if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);
        if (doc.isBlocked) throw new Error(constants.MESSAGES.ACCOUNT_BLOCKED);
        return res.success(constants.MESSAGES.DATA_FETCHED, doc);
    } catch (error) {
        next(error);
    }
};
//Update the admin details.
module.exports.updateProfile = async (req, res, next) => {
    try {
        await Validation.Admin.updateProfile.validateAsync(req.body);
        const nin = {
            $nin: [req.admin._id]
        };
        if (req.body.email) {
            const checkEmail = await Model.Admin.findOne({
                _id: nin,
                email: (req.body.email).toLowerCase(),
                isDeleted: false
            });
            if (checkEmail) throw new Error(constants.MESSAGES.EMAIL_ALREADY_IN_USE);
        }
        if (req.body.phone) {
            const checkPhone = await Model.Admin.findOne({
                _id: nin,
                countryCode: req.body.countryCode,
                phone: req.body.phone,
                isDeleted: false
            });
            if (checkPhone) throw new Error(constants.MESSAGES.PHONE_ALREADY_IN_USE);
        }
        delete req.body.password;
        const updated = await Model.Admin.findOneAndUpdate({
            _id: req.admin._id
        }, {
            $set: req.body
        }, {
            new: true
        });
        return res.success(constants.MESSAGES.PROFILE_UPDATED_SUCCESSFULLY, updated);
    } catch (error) {
        next(error);
    }
};
//Change the old password with the new one.
module.exports.changePassword = async (req, res, next) => {
    try {
        await Validation.Admin.changePassword.validateAsync(req.body);
        if (req.body.oldPassword == req.body.newPassword)
            throw new Error(constants.MESSAGES.PASSWORDS_SHOULD_BE_DIFFERENT);

        const doc = await Model.Admin.findOne({
            _id: req.admin._id
        });
        if (!doc) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);

        await doc.authenticate(req.body.oldPassword);
        await doc.setPassword(req.body.newPassword);
        await doc.save();

        return res.success(constants.MESSAGES.PASSWORD_CHANGED_SUCCESSFULLY);
    } catch (error) {
        next(error);
    }
};
//Reset password in case of forgot.
module.exports.resetPassword = async (req, res, next) => {
    try {
        await Validation.Admin.resetPassword.validateAsync(req.body);
        const doc = await Model.Admin.findOne({
            _id: ObjectId(req.body.id),
            isDeleted: false
        });
        if (!doc) throw new Error(constants.MESSAGES.USER_DATA_MISSING);
        await doc.setPassword(req.body.newPassword);
        await doc.save();
        return res.success(constants.MESSAGES.PASSWORD_RESET);
    } catch (error) {
        next(error);
    }
};
//Send link to the admin on email to reset the password.
module.exports.forgotPassword = async (req, res, next) => {
    try {
        await Validation.Admin.forgotPassword.validateAsync(req.body);
        const check = await Model.Admin.findOne({
            email: (req.body.email).toLowerCase(),
            isDeleted: false
        });
        if (check == null) throw new Error(constants.MESSAGES.ACCOUNT_NOT_FOUND);
        const link = `${process.env.BASE_PATH}/resetpassword?id=${check._id}`;
        let payload = {
            link: link,
            email: (req.body.email).toLowerCase()
        };
        await emailService.sendForgotLink(payload);
        return res.success(constants.MESSAGES.LINK_SENT);
    } catch (error) {
        next(error);
    }
};


//Add CMS data.
module.exports.addCms = async (req, res, next) => {
    try {
        //Add Cms pages data
        let dataObject = {};
        let addCms = null;
        if (req.body.privacyPolicy != null && req.body.privacyPolicy != "") dataObject.privacyPolicy = req.body.privacyPolicy;
        if (req.body.termsAndConditions != null && req.body.termsAndConditions != "") dataObject.termsAndConditions = req.body.termsAndConditions;
        if (req.body.aboutUs != null && req.body.aboutUs != "") dataObject.aboutUs = req.body.aboutUs;
        if (req.body.contactUs != null && req.body.contactUs != "") dataObject.contactUs = req.body.contactUs;
        if (req.body.faq != null && req.body.faq.length != 0) dataObject.faq = req.body.faq;

        addCms = await Model.Cms.findOneAndUpdate({}, dataObject, {
            upsert: true,
            new: true
        });
        return res.success(constants.MESSAGES.SUCCESS, addCms);


    } catch (error) {
        next(error);
    }
};
module.exports.getCms = async (req, res, next) => {
    try {
        //Fetch data without permission because cms pages need to be build for the app/web.
        const cmsData = await Model.Cms.findOne({});
        return res.success(constants.MESSAGES.DATA_FETCHED, cmsData);
    } catch (error) {
        next(error);
    }
};