module.exports = {
    AdminController: require("./AdminController/Admin"),
    DriverController: require("./DriverController/Driver"),
    UserController: require("./UserController/User"),
    VendorController: require("./VendorController/Vendor"),
    UploadController: require("./UploadController/Upload")     
};